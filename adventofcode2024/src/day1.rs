use anyhow::{Context, Result};
use std::collections::HashMap;
use std::fs::read_to_string;
use std::num::ParseIntError;
use std::{env, process};

fn parse(input: &str) -> Result<(Vec<u32>, Vec<u32>)> {
    let lists: (Vec<_>, Vec<_>) = input
        .lines()
        .flat_map(|e| e.split_once(' '))
        .map(|(a, b)| (a, b.trim_start()))
        .map(|(a, b)| (a.parse::<u32>(), b.parse::<u32>()))
        .collect();
    let llist: Result<Vec<_>, ParseIntError> = lists.0.into_iter().collect();
    let mut llist = llist.context("parsing left list")?;
    let rlist: Result<Vec<_>, ParseIntError> = lists.1.into_iter().collect();
    let mut rlist = rlist.context("parsing right list")?;
    llist.sort();
    rlist.sort();
    Ok((llist, rlist))
}

fn main() -> Result<()> {
    let args: Vec<_> = env::args().skip(1).collect();

    if args.len() != 1 {
        eprintln!("Usage: day1 <input>");
        process::exit(1);
    }
    let content = read_to_string(&args[0]).context("reading input file")?;
    let (list1, list2) = parse(&content)?;
    let result: u32 = list1.iter().zip(&list2).map(|(a, b)| a.abs_diff(*b)).sum();
    println!("Part one: {result}");

    let mut occurrences = HashMap::new();
    for e in &list2 {
        let v = occurrences.entry(e).or_insert(0);
        *v += 1;
    }
    let result: u32 = list1.iter().map(|e| e * *occurrences.entry(e).or_default()).sum();
    println!("Part two: {result}");

    Ok(())
}
