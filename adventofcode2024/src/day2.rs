use anyhow::{Context, Result};
use std::fs::read_to_string;
use std::{env, process};

struct Report(Vec<u32>);

impl Report {
    fn new(iter: impl Iterator<Item = u32>) -> Self {
        Self(Vec::from_iter(iter))
    }

    /// Return if the corresponding Report is safe or not
    /// A Report is safe is all of its levels are good.
    fn is_safe(&self) -> bool {
        let initial_order = self.0[0].cmp(&self.0[1]);
        for level in self.0.windows(2) {
            if level[0].cmp(&level[1]) != initial_order {
                return false;
            }
            let difference = level[0].abs_diff(level[1]);
            if !(1..=3).contains(&difference) {
                return false;
            }
        }
        true
    }

    /// Return if the corresponding Report is safe or not
    /// based on the Problem Dampener. That is the Report
    /// is safe if there is a single bad level in what would
    /// otherwise be a safe report.
    fn is_dampener_safe(&self) -> bool {
        if !self.is_safe() {
            for idx in 0..self.0.len() {
                let mut data = Vec::from(&self.0[0..idx]);
                data.extend(&self.0[idx + 1..]);
                let report = Report::new(data.into_iter());

                if report.is_safe() {
                    return true;
                }
            }
            false
        } else {
            true
        }
    }
}

fn parse(input: &str) -> Result<Vec<Report>> {
    let reports: Vec<_> = input
        .lines()
        .map(|e| Report::new(e.split(' ').map(|s| s.parse::<u32>().unwrap())))
        .collect();
    Ok(reports)
}

fn main() -> Result<()> {
    let args: Vec<_> = env::args().skip(1).collect();

    if args.len() != 1 {
        eprintln!("Usage: day2 <input>");
        process::exit(1);
    }
    let content = read_to_string(&args[0]).context("reading input file")?;
    let reports = parse(&content)?;

    let result = reports.iter().filter(|r| r.is_safe()).count();
    println!("Part One: {result}");

    let result = reports.iter().filter(|r| r.is_dampener_safe()).count();
    println!("Part Two: {result}");

    Ok(())
}
