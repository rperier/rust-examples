use anyhow::{Context, Result};
use std::fs::read_to_string;
use std::{env, process};

fn extract_mul(input: &str) -> Option<(u32, u32)> {
    let (left, rest) = input.split_once(',')?;
    let (right, _) = rest.split_once(')')?;
    let left: u32 = left.parse::<u32>().ok()?;
    let right: u32 = right.parse::<u32>().ok()?;

    Some((left, right))
}

fn parse_and_mul(input: &str) -> u32 {
    input
        .match_indices("mul(")
        .filter_map(|e| extract_mul(&input[e.0 + 4..]))
        .map(|(l, r)| l * r)
        .sum()
}

#[derive(PartialEq, Eq, Debug)]
struct Token {
    kind: u8,
    index: usize,
}

impl Token {
    const DONT: u8 = 0;
    const DO_: u8 = 1;
    const MUL: u8 = 2;
}

impl Ord for Token {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.index.cmp(&other.index)
    }
}

impl PartialOrd for Token {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.index.cmp(&other.index))
    }
}

fn parse_and_mul_conditional(input: &str) -> u32 {
    let mut donts: Vec<_> = input
        .match_indices("don't()")
        .map(|e| Token {
            kind: Token::DONT,
            index: e.0,
        })
        .collect();
    let dos: Vec<_> = input
        .match_indices("do()")
        .map(|e| Token {
            kind: Token::DO_,
            index: e.0,
        })
        .collect();
    let muls: Vec<_> = input
        .match_indices("mul(")
        .map(|e| Token {
            kind: Token::MUL,
            index: e.0,
        })
        .collect();

    donts.extend(dos);
    donts.extend(muls);
    donts.sort();

    let mut keep = true;
    let mut total = 0;
    for token in donts {
        match token.kind {
            Token::DONT => keep = false,
            Token::DO_ => keep = true,
            Token::MUL => {
                if keep {
                    if let Some((l, r)) = extract_mul(&input[token.index + 4..]) {
                        total += l * r;
                    }
                }
            }
            _ => {}
        }
    }
    total
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_extract_mul() {
        assert_eq!(extract_mul("5,5)"), Some((5, 5)));
        assert_eq!(extract_mul("5,5)withsomerest"), Some((5, 5)));
        assert_eq!(extract_mul("5,5"), None);
        assert_eq!(extract_mul("55)"), None);
    }
    #[test]
    fn test_parse_and_mul() {
        assert_eq!(
            161,
            parse_and_mul(
                "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))"
            )
        )
    }
    #[test]
    fn test_parse_and_mul_conditional() {
        assert_eq!(
            48,
            parse_and_mul_conditional(
                "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))"
            )
        )
    }
}

fn main() -> Result<()> {
    let args: Vec<_> = env::args().skip(1).collect();

    if args.len() != 1 {
        eprintln!("Usage: day3 <input>");
        process::exit(1);
    }
    let content = read_to_string(&args[0]).context("reading input file")?;
    let result = parse_and_mul(&content);
    println!("Part One: {result}");

    let result = parse_and_mul_conditional(&content);
    println!("Part Two: {result}");

    Ok(())
}
