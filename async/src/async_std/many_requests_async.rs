use async_std::io::{ReadExt, WriteExt};
use async_std::net;
use async_std::task;

async fn cheapo_request(host: &str, port: u16, path: &str) -> std::io::Result<String> {
    let mut socket = net::TcpStream::connect((host, port)).await?;
    let request = format!("GET {} HTTP/1.1\r\nHost: {}\r\n\r\n", path, host);

    socket.write_all(request.as_bytes()).await?;
    socket.shutdown(net::Shutdown::Write)?;

    let mut response = String::new();
    socket.read_to_string(&mut response).await?;

    Ok(response)
}

// Either use a wrapper function to take ownership of the arguments
// or use an async block

// async fn cheapo_owning_request(host: String, port: u16, path: String) -> std::io::Result<String> {
//     cheapo_request(&host, port, &path).await
// }

async fn many_requests(requests: Vec<(String, u16, String)>) -> Vec<std::io::Result<String>> {
    let mut handles = vec![];
    for (host, port, path) in requests {
        handles.push(task::spawn(async move {
            cheapo_request(&host, port, &path).await
        }));
    }
    let mut results = vec![];
    for handle in handles {
        results.push(handle.await);
    }
    results
}

fn main() -> std::io::Result<()> {
    let requests = vec![
        ("example.com".to_string(), 80, "/".to_string()),
        ("www.red-bean.com".to_string(), 80, "/".to_string()),
        ("en.wikipedia.org".to_string(), 80, "/".to_string()),
        ("fr.wikipedia.org".to_string(), 80, "/".to_string()),
        ("es.wikipedia.org".to_string(), 80, "/".to_string()),
        ("it.wikipedia.org".to_string(), 80, "/".to_string()),
        ("nl.wikipedia.org".to_string(), 80, "/".to_string()),
        ("pt.wikipedia.org".to_string(), 80, "/".to_string()),
        ("co.wikipedia.org".to_string(), 80, "/".to_string()),
        ("oc.wikipedia.org".to_string(), 80, "/".to_string())
    ];
    let results = task::block_on(many_requests(requests));

    for result in results {
        let result = result?;
        std::println!("{result}");
    }
    Ok(())
}
