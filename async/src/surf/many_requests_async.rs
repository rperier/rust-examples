use async_std::task;
use surf::{Client, Result};

async fn many_requests(urls: &[String]) -> Vec<Result<String>> {
    let client = Client::new();
    let mut handles = vec![];

    for url in urls {
        let request = client.get(&url).recv_string();
        handles.push(task::spawn(request));
    }
    let mut results = vec![];
    for handle in handles {
        results.push(handle.await);
    }
    results
}

// In term of performance it is slower than the other async many_requests,
// this is normal we have a whole HTTP stack including TLS... The purpose
// of this example here is to show how to code an async http client, nothing
// more
fn main() -> Result<()> {
    let requests = &["http://example.com".to_string(),
        "https://www.red-bean.com".to_string(),
        "https://en.wikipedia.org".to_string(),
        "https://fr.wikipedia.org".to_string(),
        "https://es.wikipedia.org".to_string(),
        "https://it.wikipedia.org".to_string(),
        "https://nl.wikipedia.org".to_string(),
        "https://pt.wikipedia.org".to_string(),
        "https://co.wikipedia.org".to_string(),
        "https://oc.wikipedia.org".to_string()
    ];
    let results = task::block_on(many_requests(requests));

    for result in results {
        let result = result?;
        std::println!("{result}");
    }
    Ok(())
}
