use std::io::prelude::*;
use std::net;

fn cheapo_request(host: &str, port: u16, path: &str) -> std::io::Result<String> {
    let mut socket = net::TcpStream::connect((host, port))?;
    let request = format!("GET {} HTTP/1.1\r\nHost: {}\r\n\r\n", path, host);

    socket.write_all(request.as_bytes())?;
    socket.shutdown(net::Shutdown::Write)?;

    let mut response = String::new();
    socket.read_to_string(&mut response)?;

    Ok(response)
}

fn many_requests(requests: Vec<(String, u16, String)>) -> Vec<std::io::Result<String>> {
    let mut results = vec![];
    for (host, port, path) in requests {
        results.push(cheapo_request(&host, port, &path))
    }
    results
}

fn main() -> std::io::Result<()> {
    let requests = vec![
        ("example.com".to_string(), 80, "/".to_string()),
        ("www.red-bean.com".to_string(), 80, "/".to_string()),
        ("en.wikipedia.org".to_string(), 80, "/".to_string()),
        ("fr.wikipedia.org".to_string(), 80, "/".to_string()),
        ("es.wikipedia.org".to_string(), 80, "/".to_string()),
        ("it.wikipedia.org".to_string(), 80, "/".to_string()),
        ("nl.wikipedia.org".to_string(), 80, "/".to_string()),
        ("pt.wikipedia.org".to_string(), 80, "/".to_string()),
        ("co.wikipedia.org".to_string(), 80, "/".to_string()),
        ("oc.wikipedia.org".to_string(), 80, "/".to_string())
    ];
    let results = many_requests(requests);

    for result in results {
        let result = result?;
        println!("{result}");
    }
    Ok(())
}
