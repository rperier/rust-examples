use anyhow::{anyhow, Context, Result};
use std::collections::BTreeMap;
use std::fs::{self, File};
use std::io::{stdout, BufWriter, Write};
use std::process;
use std::str::{self, FromStr};

pub struct BootStat {
    bootime: f32,
    history: BTreeMap<String, f32>,
}

fn boot_date() -> Result<String> {
    let mut output = String::from_utf8(process::Command::new("uptime").arg("--since").output()?
            .stdout)?;
    output.pop();
    Ok(output)
}

impl BootStat {
    pub fn update(&mut self) -> Result<()> {
        let output = process::Command::new("systemd-analyze").output()
            .context("executing command")?
            .stdout;
        let output: String = str::from_utf8(&output)?.lines().take(1).collect();

        let (firmware, total) = output.rsplit_once('=').ok_or(anyhow!("malformed output"))?;
        let firmware: Vec<_> = firmware.split_whitespace().take(4).collect();

        let total = if let Some((min, seconds)) = total.split_once("min") {
            let mut secs = f32::from_str(&seconds[1..seconds.len() - 2])
                .context("converting seconds of total time to float")?;
            secs +=
                u8::from_str(&min[1..]).context("converting min of total time to float")? as f32;
            secs
        } else {
            f32::from_str(&total[1..total.len() - 2]).context("converting total time to float")?
        };

        let firmware = f32::from_str(&firmware[3][0..firmware[3].len() - 1])
            .context("converting firmware time to float")?;

        self.bootime = total - firmware;
        Ok(())
    }

    pub fn read_from_cache() -> Result<Self> {
        let history = if fs::exists("cache")? {
            let data = fs::read("cache")?;
            bincode::deserialize(&data)?
        } else {
            BTreeMap::new()
        };
        Ok(Self { bootime: 0.0, history })
    }

    pub fn write_to_history(&mut self) -> Result<()> {
        let cache = File::create("cache")?;
        let mut cache = BufWriter::new(cache);

        self.history.insert(boot_date()?, self.bootime);

        let data = bincode::serialize(&self.history)?;
        cache.write_all(&data)?;

        Ok(())
    }

    pub fn bootime(&self) -> f32 {
        return self.bootime;
    }

    pub fn list_boots(&self) -> Result<()> {
        let mut lock = stdout().lock();
        for (k, v) in &self.history {
            writeln!(lock, "{}\t{}s", k, v)?;
        }
        Ok(())
    }

    pub fn statistics(&self) -> Result<()> {
        let min = self.history.values().min_by(|a, b| a.partial_cmp(b).unwrap())
            .ok_or(anyhow!("while looking for the min in the history"))?;
        let max = self
            .history.values().max_by(|a, b| a.partial_cmp(b).unwrap())
            .ok_or(anyhow!("while looking for the max  in the history"))?;
        let avg: f32 = self.history.values().sum::<f32>() / self.history.len() as f32;

        println!("min: {min}, avg: {avg}, max: {max}");
        Ok(())
    }
}
