use anyhow::Result;
use bootstat::BootStat;
use clap::{Parser, Subcommand};

mod bootstat;

#[derive(Parser)]
struct Args {
    #[command(subcommand)]
    command: Command,
}

#[derive(Subcommand)]
enum Command {
    Time {
        #[arg(short, long)]
        save: bool,
    },
    ListBoots {
        #[arg(short, long)]
        statistics: bool,
    },
}

fn main() -> Result<()> {
    let args = Args::parse();

    match args.command {
        Command::Time { save } => {
            let mut stats = BootStat::read_from_cache()?;
            stats.update()?;
            println!("Booted in {}s", stats.bootime());
            if save {
                stats.write_to_history()?;
            }
        }
        Command::ListBoots { statistics } => {
            let stats = BootStat::read_from_cache()?;

            if statistics {
                stats.statistics()?;
            } else {
                stats.list_boots()?;
            }
        }
    }

    Ok(())
}
