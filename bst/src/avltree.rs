use std::cmp::Ordering;
use std::mem;

pub struct AvlTree<T>(Option<Box<AvlNode<T>>>);

pub struct AvlNode<T> {
    element: T,
    height: i8,
    left: AvlTree<T>,
    right: AvlTree<T>,
}

impl<T: Ord + Copy> AvlTree<T> {
    fn height(&self) -> i8 {
        match self.0 {
            Some(ref n) => n.height,
            None => 0,
        }
    }

    fn update_height(&mut self) {
        if let Some(ref mut n) = self.0 {
            n.height = 1 + i8::max(n.left.height(), n.right.height())
        }
    }

    fn right_rotate(&mut self) {
        if self.0.is_some() {
            let mut left = self.0.as_mut().unwrap().left.0.take();
            self.0.as_mut().unwrap().left.0 = left.as_mut().unwrap().right.0.take();
            left.as_mut().unwrap().right.0 = mem::take(&mut self.0);
            left.as_mut().unwrap().left.update_height();
            left.as_mut().unwrap().right.update_height();
            self.0 = left;
            self.update_height();
        }
    }

    fn left_rotate(&mut self) {
        if self.0.is_some() {
            let mut right = self.0.as_mut().unwrap().right.0.take();
            self.0.as_mut().unwrap().right.0 = right.as_mut().unwrap().left.0.take();
            right.as_mut().unwrap().left.0 = mem::take(&mut self.0);
            right.as_mut().unwrap().right.update_height();
            right.as_mut().unwrap().left.update_height();
            self.0 = right;
            self.update_height();
        }
    }

    pub fn new() -> Self {
        Self(None)
    }

    pub fn add(&mut self, value: T) {
        let mut balancing = 0;
        match self.0 {
            Some(ref mut n) => {
                if value <= n.element {
                    n.left.add(value);
                } else {
                    n.right.add(value);
                }
                n.height = 1 + i8::max(n.left.height(), n.right.height());
                balancing = n.left.height() - n.right.height();
            }
            None => { self.0 = Some(Box::new(AvlNode { element: value, height: 1, left: AvlTree::new(), right: AvlTree::new() })) }
        }

        if balancing < -1 {
            /* Right right case  */
            if self.0.as_ref().unwrap().right.0.as_ref().is_some_and(|x| value > x.element) {
                self.left_rotate();
            /* Right left case */
            } else if self.0.as_ref().unwrap().right.0.as_ref().is_some_and(|x| value < x.element) {
                self.0.as_mut().unwrap().right.right_rotate();
                self.left_rotate();
            }
        } else if balancing > 1 {
            /* Left left case */
            if self.0.as_ref().unwrap().left.0.as_ref().is_some_and(|x| value < x.element) {
                self.right_rotate();
            /* Left right case */
            } else if self.0.as_ref().unwrap().left.0.as_ref().is_some_and(|x| value > x.element) {
                self.0.as_mut().unwrap().left.left_rotate();
                self.right_rotate();
            }
        }
    }

    pub fn remove(&mut self, value: T) {
        if let Some(ref mut n) = self.0 {
            match value.cmp(&n.element) {
                Ordering::Equal => match (&n.left.0, &n.right.0) {
                    (Some(_), Some(_)) => {
                        let mut current = &mut n.right;
                        while current.0.is_some() {
                            if current.0.as_ref().unwrap().left.0.is_none() {
                                break;
                            }
                            current = &mut current.0.as_mut().unwrap().left;
                        }
                        current.0.as_mut().unwrap().left.0 = mem::take(&mut n.left.0);
                        self.0 = current.0.take();
                    }
                    (None, Some(_)) => self.0 = n.right.0.take(),
                    (Some(_), None) => self.0 = n.left.0.take(),
                    (None, None) => self.0 = None,
                },
                Ordering::Less => n.left.remove(value),
                Ordering::Greater => n.right.remove(value),
            }
        }
    }
 }
