use bst::AvlTree;

fn main() {
    let mut tree = AvlTree::new();
    let v = [3, 2, 1];
    
    for i in v {
        tree.add(i);
    }

    println!("{:?}", tree);
}
