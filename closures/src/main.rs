use std::thread;

#[derive(Debug)]
struct City {
    name: String,
    population: i64,
    country: String,
    area: i64,
    monster_attack_risk: f32,
}

enum Statistic {
    Name,
    Population,
    Country,
    Area,
}

impl City {
    fn new(name: String, population: i64, country: String, area: i64, monster_attack_risk: f32) -> Self {
        Self { name, population, country, area, monster_attack_risk }
    }
}

fn new_cities() -> Vec<City>
{
    let paris = City::new("Paris".to_string(), 2_133_111_i64, "France".to_string(), 105_i64, 0.0);
    let tokyo = City::new("Tokyo".to_string(), 37_194_104_i64, "Japan".to_string(), 2_191_i64, 0.25);
    let delhi = City::new("Delhi".to_string(), 32_941_308_i64, "India".to_string(), 1_484_i64, 0.30);

    vec![paris, tokyo, delhi]
}

/// Helper function for sorting cities by population.
fn city_population_descending(city: &City) -> i64 {
    -city.population
}

fn city_monster_attack_risk_descending(city: &City) -> i64 {
    -(city.monster_attack_risk * 100_f32) as i64
}

/// An example of a test function. Note that the type of
/// this function is `fn(&City) -> bool`, the same as
/// the `test_fn` argument to `count_selected_cities`.
fn has_monster_attacks(city: &City) -> bool {
    city.monster_attack_risk > 0.0
}

/// Given a list of cities and a test function,
/// return how many cities pass the test.
fn count_selected_cities(cities: &[City], test_fn: fn(&City) -> bool) -> usize
{
    count_selected_cities_fixed(&cities, test_fn)
}

fn count_selected_cities_fixed<F>(cities: &[City], test_fn: F) -> usize 
    where F: Fn(&City) -> bool
{
    let mut count = 0;

    for city in cities {
        if test_fn(city) {
            count += 1;
        }
    }
    count    
}

fn sort_by_statistic(cities: &mut [City], stat: Statistic) {
    cities.sort_by_key(|city| match stat {
        Statistic::Population => -city.population,
        Statistic::Area => city.area,
        _ => 0,
    });
}

fn start_sorting_threads(mut cities: Vec<City>, stat: Statistic) -> thread::JoinHandle<Vec<City>> {
    let key_fn = move |city: &City| -> i64 {
        match stat {
            Statistic::Population => -city.population,
            Statistic::Area => city.area,
            _ => 0,
        }
    };

    thread::spawn(move || {
       cities.sort_by_key(key_fn);
       cities 
    })
}

fn main() {

    /* Introduction */

    // The helper function, city_population_descending, takes a City record and extracts the key,
    // the field by which we want to sort out data. The sort_by_key method takes this key-function
    // as a parameter.
    {
    let mut cities = new_cities();
    
    cities.sort_by_key(city_population_descending);
    
    let result: Vec<_> = cities.iter().map(|x| x.population).collect();
    assert_eq!(result, [37_194_104_i64, 32_941_308_i64, 2_133_111_i64]);
    }

    // This works fine, but it's more concise to write the helper function as a closure, an anonymous
    // function expression:
    {
    let mut cities = new_cities();

    // The closure here is |City| -city.population. It takes an argument city and returns -city.population.
    // Rust infers the argument type and return type from how the closure is used. Other examples of standard
    // library features that accept closures include:
    //   - Iterator methods such as map and filter
    //   - Threading API like thread::spawn
    //   - Some methods that conditionnally need to compute a default value, like the or_insert_with method
    //     of HashMaps entries.
    //   - etc...
    cities.sort_by_key(|city| -city.population);
    
    let result: Vec<_> = cities.iter().map(|x| x.population).collect();
    assert_eq!(result, [37_194_104_i64, 32_941_308_i64, 2_133_111_i64]);
    }

    /* Closures That Borrow */

    // First, let's repeat the opening example, but with Statistic this time. See the function sort_by_statistic().
    // In this case, when Rust creates the closure, it automatically borrows a reference to stat. It stands a reason:
    // the closure refers to stat, so it must have a reference to it. The rest is simple. The closure is subject to
    // the rules about borrowing and lifetimes that we described already. In particular, since the closure contains
    // a reference to stat, Rust won't let it outlive stat. Since the closure is only used during sorting, this example
    // is fine. In short, Rust ensures safety by using lifetimes instead of garbage collection (closure are not allocated
    // on the heap nor garbage collector as compared to other languages). Rust's way is faster: even a fast GC allocation
    // will be slower that storing stat on the stack, as Rust does in this case. 
    {
    let mut cities = new_cities();
    sort_by_statistic(&mut cities, Statistic::Population);
        
    let result: Vec<_> = cities.iter().map(|x| x.population).collect();
    assert_eq!(result, [37_194_104_i64, 32_941_308_i64, 2_133_111_i64]);
    }

    /* Closures That Steal */

    // The second example is trickier. The new thread runs in parallel with the caller. When the closure returns, the new
    // thread exits. Again the closure key_fn contains a reference to stat. But this time, Rust can't guarantee that the
    // reference is used safely. Rust therefore rejects this program:
    //
    // error[E0373]: closure may outlive the current function, but it borrows `stat`, which is owned by the current function
    //  --> src/main.rs:47:18
    //    |
    // 47 |     let key_fn = |city: &City| -> i64 {
    //    |                  ^^^^^^^^^^^^^^^^^^^^ may outlive borrowed value `stat`
    // 48 |         match stat {
    //    |               ---- `stat` is borrowed here
    //
    // In fact, there are two problems here, because cities is shared unsafely as well. Quite simply, the new thread created by
    // thread::spawn can't be expected to finish its work before cities and stat are destroyed at the end of the function.
    // The solution to both problems is the same: tell Rust to move cities and stat into the closures that use them instead of
    // borrowing references to them. So the only thing we've changed is to add the move keywords before each of the two closures.
    // The move keyword tells Rust that a closure doesn't borrow the variables it uses: it steals them.
    //
    // The first closure, key_fn, takes owernership of stat. Then the second closure takes ownership of both cities and key_fn.
    // Rust thus offers two ways for closures to get data from enclosing scopes: moves and borrowing. Really there is nothing more
    // to say than that; closures follow the same rules about moves and borrowing that we already covered already.
    {
    let mut cities = new_cities();
    cities = start_sorting_threads(cities, Statistic::Area).join().expect("failed to join thread");

    let result: Vec<_> = cities.iter().map(|x| x.area).collect();
    assert_eq!(result, [105_i64, 1_484_i64, 2_191_i64]);
    }

    /* Function and Closure Types */

    // Until now, we've seen functions and closures used as values. Naturally, this means that they have types. For example:
    // 
    // fn city_population_descending(city: &City) -> i64 {
    //  -city.population   
    // }
    //
    // This functions takes one argument (a &City) and returns an i64. It has the type fn(&City) -> i64.
    //
    // You can do all the same things with functions that you do with other values. You can store them in variables. You can use
    // all the usual Rust syntax to compute function values:
    {
    let mut cities = new_cities();

    let userprefs = "monster attack risk";
    let my_key_fn: fn(&City) -> i64 =
    if userprefs == "by population" {
        city_population_descending
    } else {
        city_monster_attack_risk_descending
    };

    cities.sort_by_key(my_key_fn);

    let result: Vec<_> = cities.iter().map(|x| x.monster_attack_risk).collect();
    assert_eq!(result, [0.30, 0.25, 0.0]);
    }

    // Structs may have function-types fields. Generic types like Vec can store scads of functions, as long as they all share the same
    // fn type. And function values are tiny: a fn value is the memory address of the function's machine code, just like a function
    // pointer in C++. A function can take another function as an argument. For example:
    {
    let cities = new_cities();
    // How many cities are at risk for monster attack?
    let mut n = count_selected_cities(&cities, has_monster_attacks);
    assert_eq!(n, 2);

    // If you're familiar with function pointer in C/C++, you'll see that Rust's function values are exactly the same thing.
    // After all this, it may come as a suprise that closures do not have the same type as functions:
    let limit = 0.25;

    // n = count_selected_cities(&cities, |city| city.monster_attack_risk > limit); // error: type mismatch

    // The second argument causes a type error. To support closures, we must change the type signature of this function. It needs
    // to look like `count_selected_cities_fixed` is defined. We have changed only the type signature of count_selected_citie, not
    // the body. The new version is generic. It takes a test_fn of any typoe F as long as F implements the special trait Fn(&City) -> bool.
    // This trait is automatically implemented by all functions and most closures that take a single &City as an argument and return a
    // Boolean value:
    //
    // fn(&City) -> bool // fn type (functions only)
    // Fn(&City) -> bool // Fn trait (both functions and closures)
    //
    // This special syntax is built into the language. The -> and return type are optional; if omitted, the return type is ().
    // The new version of `count_selected_cities`, that is `count_selected_cities_fixed` accepts either a function or a
    // closure:
    n = count_selected_cities_fixed(&cities, has_monster_attacks); // ok
    assert_eq!(n, 2);

    n = count_selected_cities_fixed(&cities, |city| city.monster_attack_risk > limit); // also ok
    assert_eq!(n, 1);
    }

    // Why didn't our first attempt work? Well, a closure is callable, but it's not a fn. The closure `|city| city.monster_attack_risk > limit`
    // has it own type that's not a fn type.
    // In fact, every closure you write has its own type, because a closure may contain data: values either borrowed or stolen from enclosing
    // scopes. This could be any number of variables, in any combination of types. So every closure has an ad hoc type created by the compiler,
    // large enough to hold that data. No two closures have exactly the same type. But every closure implements an Fn trait; the closure in our
    // example implements Fn(&City) -> i64. Since every closure has its own type, code that works with closures usually needs to be generic, likr
    // `count_selected_cities_fixed`. It's a little clunky to spell out the generic type each time, but to see the advantage of this design, just
    // read on.

    /* Closures That Kill */

    // Now suppose we have a closure that drop values. The most straightforwar way to do is to call drop():
    let my_str = "hello".to_string();
    let f = || drop(my_str);

    // When f is called, my_str is dropped.
    // So what happens if we call it twice ?
    f(); // ok
    //f(); // error: use of moved value (f is moved after is first call)

    // Of course Rust can't be fooled so easily. Rust knows this closure can't be called twice.
    // A closure that can be called only once may seem like a rather extraordinary thing, but we've been talking about ownership and lifetimes early.
    // The idea of values being used up (that is, moved) is one of the core concepts in Rust. It works the same with closures as everything else.

    /* FnOnce */

    // Let's do the same but this time with a generic function:
    
    fn call_twice<F>(closure: F) where F: Fn() {
        closure();
        closure();
    }

    // This generic function may be passed any closure that implements the trait Fn(): that is, closures that take no arguments and return (). Now
    // let's try to call this generic function with our unsafe closure `f`:
    // 253 |     let f = || drop(my_str);
    //     |             ^^      ------ closure is `FnOnce` because it moves the variable `my_str` out of its environment
    //     |             |
    //     |             this closure implements `FnOnce`, not `Fn`
    //    ...
    // 275 |     call_twice(f);
    //     |     ---------- - the requirement to implement `Fn` derives from here
    //     |     |
    //     |     required by a bound introduced by this call

    // Closures that drop values, like f, are not allowed to have Fn. They are, quite literally, no Fn at all. They implement a less powerful trait,
    // FnOnce, the trait of closures that can be called once. The first time you call a FnOnce closure, the closure itself is used up. Fn and FnOnce
    // are defined like this (Pseudocode):
    //
    // trait Fn() -> R {
    //  fn call(&self) -> R;   
    // }
    //
    // trait FnOnce() -> R {
    //  fn call_once(self) -> R;  
    // }
    //
    // Here the FnOnce trait consumes the closure, while it borrows the closure with Fn().
    // Of course calling drop on a value from a closure is rare, this example is extreme, but sometimes you will write some closure code that 
    // unintentionally uses up a value:
    //
    use std::collections::HashMap;
    let mut dict = HashMap::new();
    dict.insert("key1", 1);
    dict.insert("key2", 2);

    let debug_dump_dict = || {
        for (key, value) in dict {
            println!("{:?} - {:?}", key, value);
        }
    };
    debug_dump_dict();

    // For the same reason it cannot be called more than once because of the FnOnce trait (because dict is moved by the for loop).
    // To make this works, simply use a &dict. This fixes the error; the function is now an Fn and can be called any number of times.

    /* FnMut */

    // There is one more kind of closure, the kind that contains mutable data or mutable references.
    //
    // Rust considers non-mut value safe to share across threads. But it would not be safe to share not non-mut closures that contain
    // mut data: calling such a closure from multiple threads could lead to all sorts of race conditions as mutiple threads try to
    // read and write the same data at the same time.
    //
    // Therefore, Rust has one more category of closure, FnMut, the category of closures that write. FnMut closures are called by mut
    // reference, as if they were defined like this (Pseudocode for `Fn`, `FnMut` and `FnOnce` traits):
    //
    // trait Fn() -> R {
    //   fn call(&self) -> R;
    // }
    //
    // trait FnMut() -> R {
    //   fn call_mut(&mut self) -> R;
    // }
    //
    // trait FnOnce() -> R {
    //   fn call_once(self) -> R;
    // }
    //
    // Any closure that requires mut access to a value, but does not drop any values, is an FnMut closure. For example:

    let mut i = 0;
    let incr = || {
      i += 1; // incr borrows a mut reference to it
      println!("Ding! i is now {i}");
    };

    //call_twice(incr); // This code fails to compile

    // The way we wrote `call_twice`, it requires an Fn. Since `incr` is an FnMut and not an Fn, this code fails to compile.
    // There's an easy fix, though. Every Fn meets the requirements for FnMut, and every FnMut meets the requirements for FnOnce.
    // In reality, Fn is a subtrait of FnMut, which is a subtrait of FnOnce. This makes Fn the most exclusivee and most powerful
    // category. FnMut and FnOnce are broader categories that include closures with usage restrictions. Now, it's clear that to
    // accept the widest possible swath of closures, our `call_twice` function really ought to accept all FnMut closures, like this:

     fn call_twice_fixed<F>(mut closure: F) where F: FnMut() {
        closure();
        closure();
     }
    call_twice_fixed(incr); // ok

    // With this change, we still accept all Fn closures, and we additionnally can use call_twice on closure that mutate data:
    call_twice_fixed(|| println!("hello"));
    call_twice_fixed(|| i += 1 );
    assert_eq!(i, 4);

    /* Copy and Clone for Closures */

    // Just as Rust automatically figures out which closures can be called only once, it can figure out which closures can implement
    // Copy and Clone, and which cannot.
    //
    // As explained earlier, closures are represented as structs that contain either the values (for move closures) or references to
    // the value (for non-move closures) of the variable they capture. The rules for Copy and Clone on closures are just like the Copy
    // and Clone rules for regular structs. A non-move closure that does not mutate variables holds only shared references, which are
    // both Clone and Copy, so that closure is bot Clone and Copy as well:

    let y = 10;
    let add_y = |x| x + y;
    let copy_of_add_y = add_y;                // This closure is `Copy` and `Clone`, so ...
    let clone_of_add_y = add_y.clone();
    assert_eq!(add_y(copy_of_add_y(clone_of_add_y(22))), 52); // we can call both.

    // On the other hand, a non-move closure that does mutate values has mutable references within its internal representation. Mutable
    // references are neither Clone not Copy, so neither is a closure that uses them:
    let mut x = 0;
    let mut add_to_x = |n| { x += n; x};

    assert_eq!(add_to_x(42), 42);

    let copy_of_add_to_x = add_to_x; // this moves, rather than copies
    // assert_eq!(add_to_x(copy_of_add_to_x(1)), 2); // error: use of moved value

    // For a move closure, the rules are even simpler. If everything a move closure captures is Copy, it's Copy. If everything it captures
    // is Clone, it's Clone. For instance:
    let mut greeting = String::from("Hello, ");
    let greet = move |name| {
        greeting.push_str(name);
        println!("{}", greeting);
    };
    greet.clone()("Alfred");
    greet.clone()("Bruce");

    // This .clone()(...) syntax is a little weird, but it just means that we clone the closure and then call the clone. When greeting is used
    // in greet, it's moved into the struct that represents greet internally, because it's a move closure. So, when we clone greet, everything
    // inside it is cloned, too. There are two copies of greeting, which are each modified seperately when the clones of greet are called. This
    // isn't so useful on its own, but when you need to pass the same closure in more than one function, it can be very helpful.

    /* Callbacks */
}
