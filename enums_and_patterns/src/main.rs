use std::{cmp::Ordering, u32};
use num_enum::TryFromPrimitive;

fn compare(n: i32, m: i32) -> Ordering {
    if n < m {
        Ordering::Less
    } else if n > m  {
        Ordering::Greater
    } else {
        Ordering::Equal
    }
}

/* In memory, values of C-style enums are stored as integers. Occasionally it's useful to tell Rust
 * which integers to use. Otherwise Rust will assign the numbers for you starting at 0.
 */
#[derive(Debug, PartialEq, TryFromPrimitive)] // Debug and PartialEq for line 83, TryFromPrimitive for num_enum crate
#[repr(u16)]                                  // for num_enum crate
enum HttpStatus {
    Ok = 200,
    NotModified = 304,
    NotFound = 404,
    /* ... */
}

fn http_status_from_u32(n: u32) -> Option<HttpStatus> {
    match n {
        200 => Some(HttpStatus::Ok),
        304 => Some(HttpStatus::NotModified),
        404 => Some(HttpStatus::NotFound),
        /* ... */
        _ => None
    }
}

/* As with structs, the compiler will implement features like the == operator for you, but you
 * have to ask: */
#[derive(Copy, Clone, Debug, PartialEq, Eq)] 
enum TimeUnit {
    Seconds, Minutes, Hours, Days, Months, Years
}

/* Enums can have methods, just like structs :*/
impl TimeUnit {
    /// Return the plural noun for this time unit.
    fn plural(self) -> &'static str {
        match self {
            TimeUnit::Seconds => "seconds",
            TimeUnit::Minutes => "minutes",
            TimeUnit::Hours => "hours",
            TimeUnit::Days => "days",
            TimeUnit::Months => "months",
            TimeUnit::Years => "years"
        }
    }

    /// Return the singular noun for this time unit.
    fn singular(self) -> &'static str {
        self.plural().trim_end_matches('s')
    }
}

/// A timestamp that has been deliberately rounded off, so our program
/// says "6 months ago" instead of "February 9, 2016, at 9:49 AM".
#[derive(Copy, Clone, Debug, PartialEq)]
enum RoughTime {
    InThePast(TimeUnit, u32),
    JustNow,
    InTheFuture(TimeUnit, u32)
}

#[derive(Debug, PartialEq)]
struct Point3d {
    x: i32,
    y: i32,
    z: i32
}

#[derive(Debug, PartialEq)]
enum Shape {
    Sphere { center: Point3d, radius: f32 },
    Cuboid { corner1: Point3d, corner2: Point3d }
}

impl Shape {
    fn center(&self) -> &Point3d {
        match *self {
            Self::Sphere { ref center, .. } => {
                center
            }
            _ => { panic!("not implemented") }
        }
    }
}

// An ordered collection of `T`s.
enum BinaryTree<T> {
    Empty,
    NonEmpty(Box<TreeNode<T>>)
}

// A part of a BinatryTree.
struct TreeNode<T> {
    element: T,
    left: BinaryTree<T>,
    right: BinaryTree<T>
}
/* match performs pattern matching; in this example, the patterns are the parts that appear before
 * the => symbol. Patterns that match RoughTime values look just the expressions used to create
 * RoughTime values. This is no coincidence. Expressions produce values; patterns consume values.
 * The two use a lot of the same syntax. */
fn rough_time_to_english(rt: RoughTime) -> String {
    /* Pattern matching an enum, struct or tuple works as thought Rust is doing a simple
     * left-to-right scan, checking each component of the pattern to see if the value matches it.
     * If it doesn't, Rust moves on to the next pattern. */
    match rt {
        /* When a pattern contains simple identifiers like units and count, those become local
         * variables in the code following the pattern. Whatever is present in the value is copied
         * or moved into the new variables. */
        RoughTime::InThePast(units, count) =>
            format!("{} {} ago", count, units.plural()),
        RoughTime::JustNow =>
            format!("just now"),
        RoughTime::InTheFuture(units, 1) =>
            format!("a {} from now", units.singular()),
        RoughTime::InTheFuture(units, count) =>
            format!("{} {} from now", count, units.plural()),
    }
}

fn count_rabbits(meadow: u8) {
    match meadow {
        0 => {},
        1 => println!("A rabbit is nosing around in the clover."),
        n => println!("There are {} rabbits hopping about in the meadow", n)
    }
}

#[derive(Debug, PartialEq)]
enum Calendar {
    Gregorian,
    Chinese,
    Ethiopian
}

fn what_is_calendar(name: &str) -> Calendar {
    match name {
        "gregorian" => Calendar::Gregorian,
        "chinese" => Calendar::Chinese,
        "ethiopian" => Calendar::Ethiopian,
        // it serves as a catchal pattern like `n` above. These patterns play the same role as a
        // default case in a switch statement, matching values that don't match any of the other
        // patterns. In this case, you care about the matched value, otherwise you can use a single
        // underscore _ as a pattern, the widlcard pattern. The wildcard pattern matches any value,
        // but without storing it anywhere. Since Rust requires every match expression to handle
        // all possible values, a wildcard is often required at the end.
        other => panic!("parse_error unknown {} calendar", other)
    }
}

fn describe_point(x: i32, y: i32) -> &'static str {
    use std::cmp::Ordering::*;
    match (x.cmp(&0), y.cmp(&0)) {
        (Equal, Equal) => "at the origin",
        (_, Equal) => "on the x axis",
        (Equal, _) => "on the y axis",
        (Greater, Greater) => "in the first quadrant",
        (Less, Greater) => "in the second guadrant",
        _ => "somewhere else"
    }
}

struct Point {
    x: i32,
    y: i32
}

struct Balloon {
    location: Point 
}

fn where_is_my_balloon(balloon: Balloon) -> String {
    match balloon.location {
        Point { x: 0, y: height } =>
            format!("straight up {} meters", height),
        Point { x, y } =>  // Same as Point { x: x, y : y } but with a shorthand
            format!("at ({}m, {}m)", x, y),
    }
}

fn hsl_to_rgb(hsl: [u8; 3]) -> [u8; 3] {
    match hsl {
        [_, _, 0] => [0, 0, 0],
        [_, _, 255] => [255, 255, 255],
        _ => panic!("not implemented")
    }
}

fn greet_people(names: &[&str]) -> String {
    match names {
        [] => format!("Hello, nobody."),
        [a] => format!("Hello, {}.", a),
        [a, b] => format!("Hello, {} and {}.", a, b),
        [a, .., b] => format!("Hello, everyone from {} to {}.", a, b),
    }
}

fn dump_balloon(b: &Balloon) {
    println!("Balloon is at [{}, {}]", b.location.x, b.location.y);
}

fn dump_point(p: &Point) {
    println!("Point is at [{}, {}]", p.x, p.y);
}

fn point_to_hex(p: Point) -> Option<String>
{
    if p.x > 0 && p.y > 0 {
        Some(format!("{:X}{:X}", p.x, p.y))
    } else {
        None
    }
}

fn check_move(current_hex: &str, click: Point) -> Result<String, &str>
{
    match point_to_hex(click) {
        None => Err("That's not a game space."),
        Some(hex) if hex == current_hex  => Err("You are already there! You must click somewhere else."),
        Some(hex) => Ok(hex)
    }
}

fn main() {
    /* Example 1: Enums */
    /* Simple C-style enums are straightforward: 
     * enum Ordering {
     *  Less,
     *  Equal,
     *  Greater
     * }
     *
     * This declares a type Ordering with three possible values, called variants or constructors:
     * Ordering::Less, Ordering::Equal, Ordering::Greater. This particular enum is part of the
     * standard library, so Rust code can import it, either by itself (see the compare function
     * above) or with all its constructors:
     *
     * use std::cmp::Ordering::{self, *};
     *
     * fn compare(n: i32, m: i32) -> Ordering {
     *   if n < m {
     *     Ordering::Less
     *   } else if n > m  {
     *     Ordering::Greater
     *   } else {
     *     Ordering::Equal
     *   }
     * }
     */
    assert_eq!(compare(1, 2), Ordering::Less);
    assert_eq!(compare(1, 1), Ordering::Equal);
    assert_eq!(compare(2, 1), Ordering::Greater);

    /* By default, Rust stores C-style enums using the smallest built-in integer type that can
     * accomodate them. Most fit in a single byte (you can override Rust's choice of in-memory
     * representation by adding a #[repr] attribute to the enum): */
    use std::mem::size_of;
    assert_eq!(size_of::<Ordering>(), 1);
    assert_eq!(size_of::<HttpStatus>(), 2); // 404 doesn't fit in a u8

    /* Casting a C-style enum to an integer is allowed. */
    assert_eq!(HttpStatus::Ok as i16, 200);
    assert_eq!(HttpStatus::NotModified as i16, 304);
    assert_eq!(HttpStatus::NotFound as i16, 404);

    /* However, casting in the other direction,
     * from the integer to the enum, is not. Unlike C or C++, Rust guarantees that an enum value is
     * only ever one of the values spelled out in the enum declaration. An unchecked cast from an
     * integer type to an enum type could break this guarantee, so it's not allowed. You can either
     * write you own checked conversion: */
    assert_eq!(http_status_from_u32(200), Some(HttpStatus::Ok));
    assert_eq!(http_status_from_u32(304), Some(HttpStatus::NotModified));
    assert_eq!(http_status_from_u32(404), Some(HttpStatus::NotFound));
    assert_eq!(http_status_from_u32(666), None);

    /* or use the enum_primitive crate (no longer maintained?) or the num_enum crate */
    assert_eq!(HttpStatus::try_from(200), Ok(HttpStatus::Ok));
    assert_eq!(HttpStatus::try_from(304), Ok(HttpStatus::NotModified));
    assert_eq!(HttpStatus::try_from(404), Ok(HttpStatus::NotFound));
    assert!(HttpStatus::try_from(666).is_err());
    
    /* Enums can have methods, just like structs. */
    assert_eq!(TimeUnit::Seconds.plural(), "seconds");
    assert_eq!(TimeUnit::Minutes.plural(), "minutes");
    assert_eq!(TimeUnit::Hours.plural(), "hours");
    assert_eq!(TimeUnit::Days.plural(), "days");
    assert_eq!(TimeUnit::Months.plural(), "months");
    assert_eq!(TimeUnit::Years.plural(), "years");
    assert_eq!(TimeUnit::Seconds.singular(), "second");

    /* Example 2: Enums with Data */
    /* Some programs do not need to always display full dates and times precises to the ms.
     * Sometime it's more user-friendly to use a rough approximation, like "two months ago". We can
     * write an enum to help with that, using the RoughTime enum.*/

    /* Two of the variants in this enum, takes arguments. These are called `tuple variants`. Like
     * tuple structs, these constructors are functions that create new RoughTime values: */
    let four_score_and_seven_years_ago = RoughTime::InThePast(TimeUnit::Years, 4 * 20 + 7);
    let three_hours_from_now = RoughTime::InTheFuture(TimeUnit::Hours, 3);

    assert_eq!(four_score_and_seven_years_ago, RoughTime::InThePast(TimeUnit::Years, 87));
    assert_eq!(three_hours_from_now, RoughTime::InTheFuture(TimeUnit::Hours, 3));


    /* Enums can also have struct variants, which contain named fields, just like ordinary structs: */
    let unit_sphere = Shape::Sphere { 
        center: Point3d { x: 0, y: 0, z: 0 }, // ORIGIN
        radius: 1.0
    };

    assert_eq!(unit_sphere, Shape::Sphere { center: Point3d { x: 0, y: 0, z: 0 }, radius: 1.0 } );

    /* Example 3: Generic Enums */
    /* Enums can be generic. Two examples from the standard library are among the most used data
     * types in the language :
     *
     * enum Option<T> {
     *   None,
     *   Some(T)
     * }
     *
     * enum Result<T, E> {
     *   Ok(T),
     *   Err(E)
     * }
     *
     * The syntax for generic enums is the same as for generic structs.
     * */

    /* These lines of code instanciate a BinaryTree type that can store any number of values of
     * type &str (see definition above). */

    /* Building any particular node in this tree is straightforward: */
    use self::BinaryTree::*;

    let jupiter_tree = NonEmpty(Box::new(TreeNode {
        element: "Jupiter",
        left: Empty,
        right: Empty
    }));
    
    let mercury_tree = NonEmpty(Box::new(TreeNode {
        element: "Mercury",
        left: Empty,
        right: Empty
    }));

    /* Larger trees can be built from smaller ones :*/
    let mars_tree = NonEmpty(Box::new(TreeNode {
        element: "Mars",
        left: jupiter_tree, // transfers ownership to the parent node
        right: mercury_tree // transfers ownership to the parent node
    }));

    if let NonEmpty(t) = mars_tree {
        assert_eq!(t.element, "Mars");
    }

    /* Patterns */

    /* Example 4: Introduction to Patterns */
    /* Recall the definition of our RoughTime type from earlier in this program. Suppose you have a
     * RoughTime value and you'd like to display it on a web page or somewhere else. You need to
     * access the TimeUnit and u32 fields inside the value. Rust doesn't let you access them
     * directly, by writing rough_time.0 and rought_time.1, because after all, the value might be
     * RoughTime::JustNow, which has no fields. But then, how can you get the data out ?
     * You need to use Patterns. In this example you need a match expression : */
    assert_eq!(rough_time_to_english(four_score_and_seven_years_ago), "87 years ago");
    assert_eq!(rough_time_to_english(three_hours_from_now), "3 hours from now");

    let one_hour_from_now = RoughTime::InTheFuture(TimeUnit::Hours, 1);
    assert_eq!(rough_time_to_english(one_hour_from_now), "a hour from now");
    
    /* Example 5: Literals, Variables and Wildcards Patterns */
    /* We've shown match expressions working with enums. Other type can be matched too. When you
     * need something like a C switch statement, use match with an integer value. Integer literals
     * like 0 or 1 can serve as patterns: */
    let (meadow, meadow2, meadow3) = (0, 1, 4);
    count_rabbits(meadow);
    count_rabbits(meadow2);
    /* If there are two or more rabbits, we reach the third pattern, n. This pattern is just a
     * variable name. It can match any values, and the matched value is moved or copied into a new
     * local variable. */
    count_rabbits(meadow3);

    /* Other literals can be used as pattern too, including Bootleans, characters, and even
     * strings: */
    let calendars = ["gregorian", "chinese", "ethiopian", "rustian" ];
    assert_eq!(what_is_calendar(calendars[0]), Calendar::Gregorian);
    assert_eq!(what_is_calendar(calendars[1]), Calendar::Chinese);
    assert_eq!(what_is_calendar(calendars[2]), Calendar::Ethiopian);
    // This one panic
    //assert_eq!(what_is_calendar(calendars[3]), Calendar::Ethiopian);

    /* Example 6: Tuple and Struct Patterns */
    /* Tuple patterns match tuples. They'r useful any time you want to get multiple peieces of data
     * involved in a single match: */
    assert_eq!(describe_point(0, 0), "at the origin");
    assert_eq!(describe_point(1, 1), "in the first quadrant");
    assert_eq!(describe_point(0, 1), "on the y axis");

    /* Struct patterns use curly braces, just like struct expressions. They contain a subpattern
     * for each field: */
    let balloon1 = Balloon { location: Point { x: 0, y: 40 } };
    let balloon2 = Balloon { location: Point { x: 30, y: 40 } };
    assert_eq!(where_is_my_balloon(balloon1), "straight up 40 meters");
    assert_eq!(where_is_my_balloon(balloon2), "at (30m, 40m)");
    /* Even with the shorthand it is common to match a large struct when we only care about a few
     * fields :
     *  match get_account(id) {
     *   Some(Account {
     *        name, language, id: _, status: _, address: _, birthday: _ }) =>
     *        language.show_custom_greeting(name),
     *   ...
     *  }
     *  To avoid this, use .. to tell Rust you don't care about any of the other fields:
     *  Some(Account {name, language, .. }) =>
     *      language.show_custom_greeting(name),
     * */

    /* Example 7: Array and Slice Patterns */
    /* Array patterns match arrays. They're often used to filter out some special-case values and
     * are useful any time you're working with arrays whose values have a different meaning based
     * on position. For example, when converting hue, saturation and lightness (HSL) color values
     * to red, green, blue (RGB) color values, colors with zero lightness or full lightness are
     * just black or white.*/
    assert_eq!(hsl_to_rgb([42, 42, 0]), [0, 0, 0]);
    assert_eq!(hsl_to_rgb([42, 42, 255]), [255, 255, 255]);

    /* Slice patterns are similar, but unlike arrays, slices have variable lenghts, so slice
     * pattern match not only values but also lenght. .. in a slice pattern matches any number of
     * elements: */
    assert_eq!(greet_people(&[]), "Hello, nobody.");
    assert_eq!(greet_people(&["World!"]), "Hello, World!.");
    assert_eq!(greet_people(&["Bob", "Alice"]), "Hello, Bob and Alice.");
    assert_eq!(greet_people(&["Brian", "Raul", "Paul", "Penelope"]), "Hello, everyone from Brian to Penelope.");

    /* Example 8: Reference Patterns */
    /* Rust patterns support two features for working with references. ref pattern borrow parts of
     * a matched value. & patterns match references. Let's get introduce ref pattern first: */
    let balloon3 = Balloon { location: Point { x: 30, y: 40 } };
    match balloon3 {
        Balloon { ref location } => { // This cannot work without the ref pattern here, because location is moved from Balloon
            println!("x: {}, y: {}", location.x, location.y);
            dump_balloon(&balloon3); // balloon3 would be partially moved and cannot get borrowed
        }
    }

    /* The opposite kind of reference pattern is the & pattern. A pattern starting with & matches reference.
     * This is a bit tricky because Rust is following a pointer here, an action we usually associate with
     * the * operator, not the & operator. The thing to remember is that patterns and expressions are natural
     * opposites. In an expression, & creates a reference. In a pattern, & matches a reference. Matching 
     * a reference follows all the rules we've come to expect. Lifetimes are enforced. You can't get a mut
     * access via a shared reference. And you can't move a value out of a reference, event mut reference.
     * When we mactch &Point3d { x, y, z} , the variables x, y, z receive copies of the coordinates, leaving
     * the original Point3d value intact. It works because those fields are copyable, it would not work with
     * the following code for example:

        match friend.borrow_car() {
            Some(&Car { engine, .. }) => // error: can't move out of borrow
            ...
            None => {}
        }

        You can use a ref pattern to borrow a reference to a part. You just don't own it:
            Some(&Car { ref engine, .. }) => // ok, engine is a reference
     
     */
    match unit_sphere.center() {
        &Point3d { x, y, z } => {
            println!("unit_sphere center is {x}, {y}, {z}");
        }
    }

    /* A program can also use an & pattern to get the pointed-to characters */
    let myword = "hello, world!";
    match myword.chars().peekable().peek() {
           Some(&c) => println!("coming up: {:?}", c),
           None => println!("end of chars")
    }

    /* Example 9: Match Guards */
    /* Sometimes a match arm has additional conditions that must be met before it can be
     * considered a match. Rust provides `match guards` for this, extra conditions that
     * must be true in order for a match arm to apply, written as if CONDITION, between
     * the pattern and the arm's => token: */
    let p = Point { x: 1, y: 42 };
    let p2 = Point { x: 1, y: 43 };
    let origin = Point { x: 0, y: 0};

    /* If the pattern matches, but the condition is false, matching continues with the next arm */
    assert_eq!(check_move("12A", origin), Err("That's not a game space."));
    assert_eq!(check_move("12A", p), Err("You are already there! You must click somewhere else."));
    assert_eq!(check_move("FFFF", p2), Ok("12B".to_string()));

    /* Example 10: Matching Multiple Possibilities */
    /* A pattern of the form pat1 | pat2 matches if either subpattern matches.
     * Here `at_end` is true if peek() is None, or a Some holding a carriage return
     * a line feed. */
    let at_end = match myword.chars().peekable().peek() {
        Some(&'\r' | &'\n') | None => true,
        _ => false,
    };
    assert!(!at_end);

    /* Use ..= to match a whole range of values. Range pattern include the begin and end values,
     * so '0'..='9' matches all the ASCII digits. Rust also permits range pattern like x.., match
     * any value from x up to the maximum value of the type. However, the other varieties of
     * end-exclusive ranges are not allowed in patterns yet (0..100 or ..100) */
    let next_char = myword.chars().next().unwrap();
    match next_char {
        '0'..='9' => println!("next_char is an digit"),
        'a'..='z' => println!("next_char is letter"),
        ' ' | '\t' | '\n' => println!("next_char is a space or tab or line return"),
        _ => println!("next_char is another type")
    }

    /* Example 11: Binding with @ Patterns */
    /* Suppose you have this code:
    
        let p3 = Point { x: 30, y: 40 };
        match p3 {
            Point { x, y } => {
                dump_point(& Point {x, y});
            }
        }
    The first case unpacks a Balloon value, on to rebuild an indentical Balloon value on the next line.
    This can be rewritten to use a @ pattern:
    */
    let p3 = Point { x: 30, y: 40 };
    match p3 {
        point @ Point { .. } => {
            dump_point(& point);
        }
    }

    /* @ patterns are also useful with ranges: */
    match next_char {
        digit @ '0'..='9' => println!("next_char is an digit: {}", digit),
        letter @ 'a'..='z' => println!("next_char is letter: {}", letter),
        punc @ (' ' | '\t' | '\n') => println!("next_char is a space or tab or line return: {}", punc),
        _ => println!("next_char is another type")
    }

}
