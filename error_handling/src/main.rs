use std::fs::{self, File};
use std::io::{self, BufRead, BufReader, Write};
use std::path::Path;
use thiserror::Error;

#[derive(Error, Debug)]
#[error("{message:} ({line:}, {column})")]
struct JsonError {
    message: String,
    line: usize,
    column: usize,
}

fn pirate_share(total: u64, crew_size: usize) -> u64 {
    let half = total / 2;
    half / crew_size as u64
}

fn move_all(src: &Path, dst: &Path) -> io::Result<()> {
    for entry in src.read_dir()? {
        // opening dir could fail
        let entry = entry?; // reading dir could fail
        let dst_file = dst.join(entry.file_name());
        fs::rename(entry.path(), dst_file)?; // renaming could fail
    }
    Ok(())
}

/*
 * Example 6:
 * The following example does not work because std::num::ParseIntError cannot
 * be converted to io::Error
/// Read integers from a text file.
/// The file should have one number on each line.
fn read_numbers(file: &mut dyn BufRead) -> Result<Vec<i64>, io::Error>
{
    let mut numbers = vec![];
    for line in file.lines() {
        let line = line?;
        numbers.push(line.parse()?);
    }
    Ok(numbers)
}
*/

/* We define type alias for a Generic Error
 * This is a bit mouthful, but it represents "any error" and Send + Sync + 'static
 * makes it safe to pass between threads, which you'll often want.
 * You should also consider to use the popular crate "anyhow" which provides
 * backtraces generation and provides types like GenericError and GenericResult
 * with some nice additional features. */
type GenericError = Box<dyn std::error::Error + Send + Sync + 'static>;
//type GenericResult<T> = Result<T, GenericError>;

/// Read integers from a text file.
/// The file should have one number on each line.
fn read_numbers(file: &mut dyn BufRead) -> Result<Vec<i64>, GenericError> {
    let mut numbers = vec![];
    for line in file.lines() {
        let line = line?;
        numbers.push(line.parse()?);
    }
    Ok(numbers)
}

/// Read integers from a text file.
/// The file should have one number on each line.
fn any_read_numbers(file: &mut dyn BufRead) -> anyhow::Result<Vec<i64>> {
    let mut numbers = vec![];
    for line in file.lines() {
        let line = line?;
        numbers.push(line.parse()?);
    }
    Ok(numbers)
}

fn json_fake_parser(input: &str) -> Result<(), JsonError> {
    if input.starts_with('[') {
        return Ok(());
    }

    Err(JsonError {
        message: "Expected '[' at start of array".to_string(),
        line: 0,
        column: 0,
    })
}

fn main() {
    /* Example 1: Panic */
    let myshare = pirate_share(500, 2);
    assert_eq!(myshare, 125);

    /* This will trigger a panic (divide by zero) */
    //let fake = pirate_share(500, 0);
    //assert_eq!(fake, 1);

    /* Example 2: Result */
    /* A common way of handling Result is by using match */
    let file = match File::open("Cargo.toml") {
        Ok(f) => f,
        Err(e) => {
            eprintln!("error: {}", e);
            std::process::exit(1);
        }
    };

    /* Example 3: Result<T, E> methods for error handling */
    let mut v = [5, 4, 1, 3, 2];
    v.sort();

    let r = v.binary_search(&4);
    assert_eq!(r, Ok(3));
    assert!(r.is_ok());
    assert!(!r.is_err());

    let s = v.binary_search(&666);
    assert_eq!(s, Err(5));
    assert!(!s.is_ok());
    assert!(s.is_err());

    let t = v.binary_search(&666).unwrap_or(0);
    assert_eq!(t, 0);

    /* Example 4: Printing errors */
    match File::open("~/not-found") {
        Ok(_v) => {}
        Err(err) => {
            println!("error: {}", err);
            println!("error: {:?}", err);
        }
    }

    /* Example 5: Propagating Errors */
    /* Use the ? operator to propagate error to the enclosing function, it works with
     * either Result<T,E> or with an Option type */
    if let Err(err) = move_all(Path::new("~/does-not-exist"), Path::new("cheh")) {
        eprintln!("Something went wrong while moving file: {err:?}");
    }

    /* Example 6: Working with multiple errors types */
    let mut buf = BufReader::new(file);
    match read_numbers(&mut buf) {
        Ok(_v) => {}
        Err(err) => {
            println!("error: {}", err);
            println!("error: {:?}", err);
        }
    }

    /* Example 7: Working with multiple errors types (like Example 6) but with the crate anyhow */
    let f = File::open("src/main.rs").unwrap(); // Get the value directly, panic if there is an error
    let mut bufread = BufReader::new(f);
    match any_read_numbers(&mut bufread) {
        Ok(_v) => {}
        Err(err) => {
            println!("error: {}", err);
            println!("error: {:?}", err);
        }
    }

    /* Example 8: Dealing with Errors that "can't happen" */
    assert_eq!("666".parse::<u64>().unwrap(), 666);
    assert_eq!(
        "666"
            .parse::<u64>()
            .expect("something went wrong that normally could not happen"),
        666
    );

    /* Example 9: Ignoring Errors */

    // This line would report warning: unused result
    //writeln!(std::io::stderr(), "reporting an error message");
    // The idiom let _ = ... is used to silence this warning
    let _ = writeln!(std::io::stderr(), "reporting an error message");

    /* Example 10: Declaring a Custom Error Type */
    /* Use thiserror crates to automatically handling std::error::Error inheritance and
     * define fmt::Display for formatting*/
    assert!(json_fake_parser("[hello").is_ok());
    // This will return an JSonError and panic
    //json_fake_parser("hello").unwrap();
}
