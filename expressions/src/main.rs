use rand::random;
use std::cmp::Ordering;
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader, Error};

fn main() -> Result<(), Error> {  /* Return type is for  For Example 5*/
    let args: Vec<String> = env::args().collect();

    /* Example 1: An match expression can be used to declare a variable */
    let name = match env::var("NAME") {
        Ok(val) => val,
        _ => "Unknown".to_string(),
    };
    assert!(!name.is_empty());
    println!("{name}");

    /* Example 2: An if expression can be used to declare a variable */
    let arg = if args.len() >= 2 {
        args[1].clone()
    } else {
        "Unknown".to_string()
    };
    assert!(!arg.is_empty());
    println!("{arg}");

    /* Example 3: An match expression can be passed as function or macro argument */
    println!("HOME = {}",
        match env::var("HOME") {
            Ok(val) => val,
            _ => "Undefined".to_string(),
        }
    );

    /* Example 4: A let declaration can declare a variable without initializing it, it can be
     * initialized with a later assignment */
    let val;
    //println!("{val}"); // but it cannot be used until it is initialized
    if random::<u8>() % 2 == 1 {
        val = "odd";
    } else {
        val = "pair";
    }
    println!("{val}");

    /* Example 5: The seconde let declaration supersedes the first one for the rest of the block */
    let file = File::open("Cargo.toml")?;
    let buf = BufReader::new(file);
    for line in buf.lines() {
        let line = line?;
        println!("{line}");
    }

    /* Example 6: A block can also contain item declaration (any declaration that could appear
     * globally inside the programm or inside a module such as fn, struct or use ) */
    let mut v = [5, 4, 1, 3, 2];

    fn sort_by_example(a: &u32, b: &u32) -> Ordering {
        a.cmp(b)
    }

    v.sort_by(sort_by_example);
    assert_eq!(v, [1, 2, 3, 4, 5]);

    /* Example 7: There is one more if form, the if let expression */
    if let Ok(val) = env::var("TEST") {
        println!("TEST defined with {val}");
    } else {
        println!("Undefined");
    }

    /* Example 8: Loops */
    {
        let mut i = 0;
        while i < 3 {
            i += 1;
        }
        assert_eq!(i, 3);
    }

    {
        let mut i = 0;
        for _i in 1..4 {
            i = _i;
        }
        assert_eq!(i, 3);
    }

    {
        let mut optional = Some(0);
        while let Some(i) = optional {
            if i >= 3 {
                optional = None
            } else {
                optional = Some(i + 1);
            }
        }
        assert_eq!(optional, None);
    }

    /* Example 9: Loop are expression in Rust, but the value of a while or for loop is always ().
     * A loop expression can produce a value if you specify one */
    let mut i = 0;
    let num = loop {
        if random::<u8>() % 4 == 0 {
            break i
        }
        i += 1;
    };
    assert_eq!(num, i);
    println!("{num}");

    /* Example 10: Type Casts */
    /* Converting a value from one type to another usually requires an explicit cast in Rust */
    let x = 17;
    let index = x as usize;

    assert_eq!(index, 17);

    /* Example 11: Closures */
    let is_even = |x| x % 2 == 0; // Rust infers the argument types and return type

    //let is_even = |x: u64| -> bool { x % 2 == 0 }; // You can also write them out explicitly, like
    //a function. If you specify the return type, then the body of the closure must be a block

    assert_eq!(is_even(14), true);

    Ok(())
}
