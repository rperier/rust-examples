use anyhow::Result;
use colored::*;
use fern_sim::{run_simulation, Fern};
use std::{env, process, time::Duration, thread };
use std::str::FromStr;
use std::io::{stdout, Write};

fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    let mut fern = Fern {
        size: 1.0,
        growth_rate: 0.001,
    };

    if args.len() != 2 {
        eprintln!("fern_term - Grow a fern and display its size");
        eprintln!("{} {} {}", "Usage".green(), "fern_term".cyan().bold(),
                              "<days>".cyan());
        process::exit(1);
    }
    let days = usize::from_str(&args[1])?;

    for d in 1..=days {
        run_simulation(&mut fern, 1);
        print!("\rFern growth simulator: {} ({} days)", fern.size, d);
        stdout().flush()?;
        thread::sleep(Duration::from_millis(125));
    }

    Ok(())
}
