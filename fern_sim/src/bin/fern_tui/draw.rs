use crossterm::{terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen,
    LeaveAlternateScreen}, ExecutableCommand
};
use ratatui::{
    prelude::CrosstermBackend, widgets::canvas::*, widgets::Block, widgets::Borders, Frame,
    Terminal
};
use std::io::{self, stdout, Stdout};

pub fn init_terminal() -> io::Result<Terminal<CrosstermBackend<Stdout>>> {
    enable_raw_mode()?;
    stdout().execute(EnterAlternateScreen)?;
    Terminal::new(CrosstermBackend::new(stdout()))
}

pub fn restore_terminal() -> io::Result<()> {
    disable_raw_mode()?;
    stdout().execute(LeaveAlternateScreen)?;
    Ok(())
}

pub fn draw(frame: &mut Frame, size: f64, days: usize) {
    use ratatui::prelude::Color;
    let area = frame.size();

    frame.render_widget(
        Canvas::default()
            .block(Block::default().title("Fern growth simulator: ".to_owned() + 
                                          &usize::to_string(&days) + " days")
            .borders(Borders::ALL))
            .x_bounds([-180.0, 180.0])
            .y_bounds([-90.0, 90.0])
            .paint(|ctx| {
                // Trunk
                let growth = (1.0 * size).min(40.0);
                ctx.draw(&Line {
                    x1: 0.0,
                    y1: 0.0,
                    x2: 0.0,
                    y2: growth,
                    color: Color::Rgb(139, 69, 19),
                });
                ctx.draw(&Line {
                    x1: 1.0,
                    y1: 0.0,
                    x2: 1.0,
                    y2: growth,
                    color: Color::Rgb(139, 69, 19),
                });
                if growth < 10.0 {
                    return;
                }
                ctx.draw(&Line {
                    x1: 0.0,
                    y1: 10.0,
                    x2: -20.0,
                    y2: 20.0,
                    color: Color::Green,
                });
                ctx.draw(&Line {
                    x1: 1.0,
                    y1: 10.0,
                    x2: 20.0,
                    y2: 20.0,
                    color: Color::Green,
                });
                if growth < 15.0 {
                    return;
                }
                ctx.draw(&Line {
                    x1: 1.0,
                    y1: 15.0,
                    x2: -20.0,
                    y2: 25.0,
                    color: Color::Green,
                });
                ctx.draw(&Line {
                    x1: 1.0,
                    y1: 15.0,
                    x2: 20.0,
                    y2: 25.0,
                    color: Color::Green,
                });
                if growth < 20.0 {
                    return;
                }
                ctx.draw(&Line {
                    x1: 1.0,
                    y1: 20.0,
                    x2: -20.0,
                    y2: 30.0,
                    color: Color::Green,
                });
                ctx.draw(&Line {
                    x1: 1.0,
                    y1: 20.0,
                    x2: 20.0,
                    y2: 30.0,
                    color: Color::Green,
                });
                if growth < 25.0 {
                    return;
                }
                ctx.draw(&Line {
                    x1: 1.0,
                    y1: 25.0,
                    x2: -20.0,
                    y2: 35.0,
                    color: Color::Green,
                });
                ctx.draw(&Line {
                    x1: 1.0,
                    y1: 25.0,
                    x2: 20.0,
                    y2: 35.0,
                    color: Color::Green,
                });
            }),
        area,
    );
}
