use anyhow::Result;
use colored::*;
use crossterm::event::{self, KeyCode, KeyEventKind};
use fern_sim::{run_simulation, Fern};
use std::{env, process, time::Duration};
use std::str::FromStr;

mod draw;
use draw::{init_terminal, restore_terminal, draw};

fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    let mut fern = Fern {
        size: 1.0,
        growth_rate: 0.001,
    };

    if args.len() != 2 {
        eprintln!("fern_tui - Grow a fern and draw it on the terminal");
        eprintln!("{} {} {}", "Usage".green(), "fern_tui".cyan().bold(),
                              "<days>".cyan());
        process::exit(1);
    }
    let days = usize::from_str(&args[1])?;

    let mut terminal = init_terminal()?;
    terminal.clear()?;

    let mut d = 0;
    loop {
        if d < days {
            d += 1;
        }

        run_simulation(&mut fern, 1);
        terminal.draw(|frame| draw(frame, fern.size, d))?;

        /* Quit on 'q' */
        if event::poll(Duration::from_millis(125))? {
            if let event::Event::Key(key) = event::read()? {
                if key.kind == KeyEventKind::Press && key.code == KeyCode::Char('q') {
                    break;
                }
            }
        }
    }

    restore_terminal()?;
    Ok(())
}
