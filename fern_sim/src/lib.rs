//! Simulate the growth of ferns, from the level of
//! individual cells on up.
//!
//! Example:
//!
//!     use fern_sim::{run_simulation, Fern};
//!
//!     fn main() {
//!         let mut fern = Fern {
//!             size: 1.0,
//!             growth_rate: 0.001,
//!         };
//!         run_simulation(&mut fern, 10);
//!         println!("Fern size: {}", fern.size);
//!     }

pub struct Fern {
    pub size: f64,
    pub growth_rate: f64,
}

impl Fern {
    /// Simulate a fern growing for one day.
    fn grow(&mut self) {
        self.size *= 1.0 + self.growth_rate;
    }
}

/// Run a fern simulation for some number of days.
/// The [`Fern`] internal state is updated with its new size.
pub fn run_simulation(fern: &mut Fern, days: usize) {
    for _ in 0..days {
        fern.grow();
    }
}
