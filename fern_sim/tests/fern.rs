use fern_sim::{Fern, run_simulation};

#[test]
fn test_run_simulation() {
    let mut fern = Fern {
        size: 1.0,
        growth_rate: 0.001,
    };

    run_simulation(&mut fern, 0);
    assert_eq!(fern.size, 1.0);

    run_simulation(&mut fern, 1);
    assert_eq!(fern.size, 1.001);

    run_simulation(&mut fern, 2);
    assert_eq!(fern.size, 1.0030030009999995);
}
