#include <stdio.h>
#include <stdlib.h>

static int fibo_rec (int n, int a , int b)
{
	if (n <= 1)
		return a;
	return fibo_rec (n - 1, a + b, a);
}

static int fibo(int n)
{
	return fibo_rec(n, 1, 0);
}

int main (int argc, char *argv[])
{
	char *endptr;

	if (argc != 2) {
		fprintf(stderr, "fibo <NUMBER>\n");
		return 1;
	}
	int n = strtol(argv[1], &endptr, 10);
	if (*endptr != '\0') {
		fprintf(stderr, "Error while parsing argument 1\n");
		return 1;
	}
	printf("fibo(%d) = %d\n", n, fibo(n));
	return 0;
}
