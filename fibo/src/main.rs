#![feature(test)]
use std::env;
use std::str::FromStr;

#[cfg(test)]
mod tests {
    extern crate test;
    use super::*;
    use test::{black_box, Bencher};

    #[bench]
    fn bench_fibo_rec(b: &mut Bencher) {
        b.iter(|| {
            black_box(fibo_rec(black_box(32)));
        });
    }

    #[bench]
    fn bench_fibo_rec_terminal(b: &mut Bencher) {
        b.iter(|| {
            black_box(fibo(black_box(32)));
        });
    }

    #[bench]
    fn bench_fibo_iterator(b: &mut Bencher) {
        b.iter(|| {
            black_box(fibonacci(black_box(32)));
        });
    }
}

fn fibo_rec_term(n: usize, a: usize, b: usize) -> usize {
    match n {
        0 | 1 => a,
        _ => fibo_rec_term(n - 1, a + b, a),
    }
}

fn fibo_rec(n: usize) -> usize {
    match n {
        0 | 1 => n,
        _ => fibo_rec(n - 1) + fibo_rec(n - 2),
    }
}

fn fibo(n: usize) -> usize {
    fibo_rec_term(n, 1, 0)
}

fn fibonacci(n: usize) -> usize {
    let mut state = (0, 1);

    std::iter::from_fn(move || {
        state = (state.1, state.0 + state.1);
        Some(state.0)
    })
    .take(n)
    .last()
    .unwrap()
}

fn main() {
    if env::args().len() < 2 {
        eprintln!("Usage: fibo NUMBER");
        std::process::exit(1);
    }

    let args: Vec<String> = env::args().collect();
    let n = match usize::from_str(&args[1]) {
        Ok(v) => v,
        Err(e) => {
            eprintln!("Error while parsing argument 1: {:?}", e);
            std::process::exit(1);
        }
    };

    println!("fibo({n}) = {}", fibonacci(n));
}
