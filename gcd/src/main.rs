use std::str::FromStr;
use std::env;

fn gcd(mut n: u64, mut m: u64) -> u64 {
    assert!(n != 0 && m != 0); 
    while m != 0 {
        if m < n {
            std::mem::swap(&mut m, &mut n);
        }
        m %= n;
    }
    n
}

#[test]
fn test_gcd() {
    assert_eq!(gcd(14, 15), 1);
    assert_eq!(gcd(2 * 3 * 5 * 11 * 17, 3 * 7 * 11 * 13 * 19), 3 * 11);
}

fn main() {
    if env::args().len() < 3 {
        eprintln!("Usage: gcd NUMBER...");
        std::process::exit(1);
    }

    let args: Vec<String> = env::args().collect();
    let n = u64::from_str(&args[1]).expect("error parsing argument 1");
    let m = u64::from_str(&args[2]).expect("error parsing argument 2");

    println!("gcd({n}, {m}) = {}", gcd(n, m));
}
