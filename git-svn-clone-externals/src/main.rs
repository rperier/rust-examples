use std::process::Command;
use regex::Regex;

fn main() {
    //let output = Command::new("git").arg("svn").arg("show-externals").output().expect("cmd failed");
    let output = Command::new("cat").arg("/home/gf6145/output.txt").output().expect("cmd failed");
    let mut lines: Vec<&str> = std::str::from_utf8(&output.stdout).expect("wrong format").lines().collect();
    let re = Regex::new(r#"([^\^#"]+)/([^$]+)"#).expect("wrong regex");

    lines.retain(|&s| !s.starts_with('#') && !s.is_empty() && !s.contains('#'));
    for line in lines {
        let caps = re.captures(line).unwrap(); 
        let upperdir = &caps.get(1).unwrap().as_str()[1..];
        let uriandsubdir = &caps.get(2).unwrap().as_str()[2..];
        let (uri, subdir) = uriandsubdir.split_at(uriandsubdir.rfind(' ').unwrap());
        let mut absuri = String::from("https://tsfe-svn/svn/");
        if uri.starts_with("..") {
            absuri += &uri[2..];
        } else if uri.starts_with("/..") {
            absuri += &uri[3..];
        } else {
            absuri += &("logiciel/".to_owned() + uri);
        }
        //let svncmd = String::from("svn co ") + &absuri + " " + upperdir + "/" + subdir.trim();
        
        Command::new("git").arg("svn").arg("clone").arg(absuri.trim()).arg(upperdir.to_owned() + "/").arg(subdir.trim()).arg("-r").arg("HEAD").status().expect("svn clone failed");
    }
}
