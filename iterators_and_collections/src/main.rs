use std::iter::Iterator;

struct I32Range {
    start: i32,
    end: i32,
}

impl I32Range {
    fn new(start: i32, end: i32) -> Self {
        Self { start, end }
    }
}

impl Iterator for I32Range {
    type Item = i32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.start >= self.end {
            return None;
        }

        let result = Some(self.start);
        self.start += 1;
        result
    }
}

fn main() {
    /* Iterators */

    /* Implementing your own Iterator */

    // Suppose we have the following range type (implified from the standard library
    // std::ops::Range<T> type).
    let r = I32Range::new(0, 14);
    let mut pi = 0.0;
    let mut numerator = 1.0;

    // Iterating over an I32Rane requires two pieces of state: The current value and the limit at
    // which the iteration should end. This happens to be a nice fit for the I32Range type itself,
    // using start as the next value, and the end as the limit. So you can implement Iterator like
    // above.

    // Of course, a for loop uses IntoIterator::into_iter() to convert its operand into an iterator.
    // But the standard library provides a blanket implementation of IntoIterator for every type that
    // implements Iterator, so I32Range is ready for use:
    for k in r {
        pi += numerator / (2 * k + 1) as f64;
        numerator /= -3.0;
    }
    pi *= f64::sqrt(12.0);

    // IEEE 754 specifies this result exactly.
    assert_eq!(pi as f32, std::f32::consts::PI);

    /* Iterator Adapters */

    // maps and filter
    let text = "   ponies   \n   giraffes\niguanas   \nsquid".to_string();
    let v: Vec<&str> = text.lines().map(str::trim).collect();

    assert_eq!(v, ["ponies", "giraffes", "iguanas", "squid"]);

    let v: Vec<&str> = text
        .lines()
        .map(str::trim)
        .filter(|s| *s != "iguanas")
        .collect();
    assert_eq!(v, ["ponies", "giraffes", "squid"]);

    // filter_map and flat_map
    use std::str::FromStr;

    let text = "1\nfrond .25 289\n3.1415 estuary\n";
    for number in text
        .split_whitespace()
        .filter_map(|w| f64::from_str(w).ok())
    {
        println!("{}", number.sqrt());
    }

    use std::collections::HashMap;

    let mut major_cities = HashMap::new();
    major_cities.insert("Japan", vec!["Tokyo", "Kyoto"]);
    major_cities.insert("The United States", vec!["Portland", "Nashville"]);
    major_cities.insert("Brazil", vec!["Sao Paulo", "Brasilia"]);
    major_cities.insert("Kenya", vec!["Nairobi", "Mombasa"]);

    let countries = ["Japan", "Brazil", "Kenya"];
    for &city in countries.iter().flat_map(|country| &major_cities[country]) {
        println!("{city}");
    }

    // flatten and flat_map
    let words = ["alpha", "beta", "gamma"];

    // chars() returns an iterator here
    let merged: String = words.iter().map(|s| s.chars()).flatten().collect();

    assert_eq!(merged, "alphabetagamma");

    let word = "an example";
    let result: String = word.chars().flat_map(char::to_uppercase).collect();
    assert_eq!(result, "AN EXAMPLE");

    // take and take_while
    let message = "To: jimb\r\n\
    From: superego <editor@domain.com>\r\n\
    \r\n\
    Did you get any writing done today?\r\n\
    When will you stop wasting time plotting fractals?\r\n";

    // This takes "To: jimb" from the message
    let first_bytes: String = message.chars().take(8).collect();
    assert_eq!(first_bytes, "To: jimb");

    println!("header:");
    for header in message.lines().take_while(|l| !l.is_empty()) {
        println!("{header}");
    }

    // skip and skip_while

    // This skips "To: jimb" from the message
    let other_bytes: String = message.chars().skip(8).collect();
    println!("{other_bytes}");

    println!("body:");
    for body in message.lines().skip_while(|l| !l.is_empty()).skip(1) {
        println!("{body}");
    }

    // peakable

    // Creates an iterator which can use the `peek` and `peek_mut` methods to look at the next
    // element of the iterator without consuming it.
    // Calling peek tries to draw the next item from the underlying iterator, and if there is one,
    // cache it until the next call to `next`. All the other Iterator methods on `Peekable` (the
    // Iterator type return by `peekable`) know about this cache.
    use std::iter::Peekable;

    fn parse_number<I>(tokens: &mut Peekable<I>) -> u32
    where
        I: Iterator<Item = char>,
    {
        let mut n = 0;

        loop {
            match tokens.peek() {
                Some(r) if r.is_digit(10) => {
                    n = n * 10 + r.to_digit(10).unwrap();
                }
                _ => return n,
            }
            tokens.next();
        }
    }

    let mut chars = "226153980,1766319049".chars().peekable();
    assert_eq!(parse_number(&mut chars), 226153980);
    // Look, `parse_number` did not consume the comma! So we will.
    assert_eq!(chars.next(), Some(','));
    assert_eq!(parse_number(&mut chars), 1766319049);
    assert_eq!(chars.next(), None);

    // fuse

    // Once an Iterator has returned None, the trait does not specify how it ought to behave if you
    // call its next method again. Most iterators just return None again, but not all. If your code
    // counts on that behavior, you may be in for a surprise. The fuse adapter takes any iterator
    // and produces one that will definitely continue to return None once it as done so the first
    // time:
    struct Flaky(bool);

    impl Iterator for Flaky {
        type Item = &'static str;

        fn next(&mut self) -> Option<Self::Item> {
            if self.0 {
                self.0 = false;
                Some("totally the last item")
            } else {
                self.0 = true; // D'oh!
                None
            }
        }
    }

    let mut flaky = Flaky(true);
    assert_eq!(flaky.next(), Some("totally the last item"));
    assert_eq!(flaky.next(), None);
    assert_eq!(flaky.next(), Some("totally the last item"));

    let mut flaky = Flaky(true).fuse();
    assert_eq!(flaky.next(), Some("totally the last item"));
    assert_eq!(flaky.next(), None);
    assert_eq!(flaky.next(), None);

    // Reversible Iterators and rev

    // Both require DoubleEndedIterator: https://doc.rust-lang.org/std/iter/trait.DoubleEndedIterator.html
    let bee_parts = ["head", "thorax", "abdomen"];
    let mut iter = bee_parts.iter();

    assert_eq!(iter.next(), Some(&"head"));
    assert_eq!(iter.next_back(), Some(&"abdomen"));
    assert_eq!(iter.next(), Some(&"thorax"));

    assert_eq!(iter.next_back(), None);
    assert_eq!(iter.next(), None);

    let meals = ["breakfast", "lunch", "dinner"];
    let mut iter = meals.iter().rev();

    assert_eq!(iter.next(), Some(&"dinner"));
    assert_eq!(iter.next(), Some(&"lunch"));
    assert_eq!(iter.next(), Some(&"breakfast"));

    // chain

    // The chain adapter appends one iterator to another.
    
    let v: Vec<i32> = (1..4).chain([20, 30, 40]).collect();
    assert_eq!(v, [1, 2, 3, 20, 30, 40]);

    // A chain iterator is reversible, if both of its underlying iterators are:
    let v: Vec<i32> = (1..4).chain([20, 30, 40]).rev().collect();
    assert_eq!(v, [40, 30, 20, 3, 2, 1]);
    
    // enumerate
    let v = vec!['A', 'B', 'C'];
    let e: Vec<_> = v.iter().enumerate().collect();
    assert_eq!(e, [(0, &'A'), (1, &'B'), (2, &'C')]);

    // zip
    let v: Vec<_> = (0..).zip("ABCD".chars()).collect();
    assert_eq!(v, vec![(0, 'A'), (1, 'B'), (2, 'C'), (3, 'D')]);

    // The argument to zip can be any iterable:
    use std::iter::repeat;

    let endings = ["once", "twice", "chicken soup with rice"];
    let rhyme: Vec<_> = repeat("going")
        .zip(endings)
        .collect();
    assert_eq!(rhyme, vec![("going", "once"), ("going", "twice"), ("going", "chicken soup with rice")]);

    // by_ref
    // cloned, copied
    // cycle

    /* Consuming Iterators */

    /* Collections */

    // VecDeque<T>

    // Vec supports efficiently adding and removing elements only at the end. When a program needs
    // a place to store values that are "waiting in line", Vec can be slow.
    // VecDeque<T> is a deque (pronounced "deck"), a double-ended queue. It supports efficient add
    // and remove operations at both the frondt and the back. The "default" usage of this type as
    // a queue is to use push_back to add to the queue, and pop_front to remove from the queue.
    use std::collections::VecDeque;

    let mut deq = VecDeque::from([-1, 0, 1]);
    deq.push_back(2);

    assert_eq!(deq.front(), Some(&-1));
    assert_eq!(deq.back(), Some(&2));

    assert_eq!(deq[0], -1);
    assert_eq!(deq[1], 0);
    assert_eq!(deq[2], 1);
    assert_eq!(deq[3], 2);

    assert_eq!(deq.pop_front(), Some(-1));
    assert_eq!(deq.pop_front(), Some(0));
    assert_eq!(deq.pop_front(), Some(1));
    assert_eq!(deq.pop_front(), Some(2));
    assert_eq!(deq.pop_front(), None);

    // BinaryHeap<T>

    // A BinaryHeap is a collection whose elements are kept loosely organized so that the greatest
    // value always bubbles up to the front of the queue.
    // See https://doc.rust-lang.org/std/collections/struct.BinaryHeap.html for the logic Error
    // use and undefined behaviors.
    use std::collections::BinaryHeap;

    let mut heap = BinaryHeap::from([2, 3, 8, 6, 9, 5, 4]);

    // The value 9 is at the top of the heap
    assert_eq!(heap.peek(), Some(&9));
    assert_eq!(heap.pop(), Some(9));

    // Removing the value 9 also rearranges the other elements slightly so that 8 is now at the
    // front, and so on
    assert_eq!(heap.pop(), Some(8));
    assert_eq!(heap.pop(), Some(6));
    assert_eq!(heap.pop(), Some(5));

    // HashMap<K,V> and BTreeMap<K, V>

    // Use an HashMap:
    // when want to associate arbitrary keys with an arbitrary value.
    // When you don't care about keys order in the map
    // When You want a cache.
    // When You want a map, with no extra functionality.
    // mostly ~O(1) operations but extra in space complexity

    let mut solar_distance = HashMap::from([
        ("Mercury", 0.4),
        ("Venus", 0.7),
        ("Earth", 1.0),
        ("Mars", 1.5),
    ]);

    solar_distance.insert("Solar", 0.0);

    // Iterators visit all key-value pairs in arbitrary order.

    // Iterables visit all key-value pairs in arbitrary order.
    for (k, v) in &solar_distance {
        println!("{k} -> {v}");
    }

    // Key Iterators visit all keys in arbitrary order.
    for k in solar_distance.keys() {
        println!("{k}");
    }
 
    // Value Iterators visit all values in arbitrary order.
    for v in solar_distance.values() {
        println!("{v}");
    }

    // Both HashMap and BTreeMap have a corresponding Entry type. The point of entries
    // is to eliminate redundant map lookups. For example:
    //
    // // Do we already have a record for this distance 
    // if solar_distance.contains_key("Jupiter") {
    //    // No: Create one.
    //    solar_distance.insert("Jupiter", f64::Default);
    // }
    // // Now a record definitively exists.
    // let record = solar_distance.get_mut("Jupiter").unwrap();
    // record = 5.2;

    // This works fine, but it access sorlar_distance two or three times, doing the same lookup
    // each time.
    // The idea with entries is that we do the lookup just once, producing an Entry value that is
    // then used for all subsequent operations. This one-liner equivalent to all of the preceding
    // code, except that it does the lookup only once.
    let record = solar_distance.entry("Jupiter").or_default();
    *record = 5.2;

    for (k, v) in &solar_distance {
        println!("{k} -> {v}");
    }

    // Use a BTreeMap:
    // When you want a map sorted by its keys.
    // When you want to be able to get a range of entries on-demand.
    // When you’re interested in what the smallest or largest key-value pair is.
    // When you want to find the largest or smallest key that is smaller or larger than something.
    // O(log(n)) for most operations (except append which is O(n+m))

    use std::collections::BTreeMap;

    let mut history = BTreeMap::from([
        ("2024-09-25 1:00:00", 2.0), 
        ("2024-09-23 1:00:00", 0.0), 
        ("2024-09-26 1:00:00", 3.0),
        ("2024-09-24 1:00:00", 1.0), 
    ]);

    let record = history.entry("2024-08-22 0:00:00").or_insert(4.0);
    assert_eq!(*record, 4.0);

    // Iterators visit all entries of the map, sorted by key


    // Iterables visit all entries of the map, sorted by key
    for (k, v) in &history {
        println!("{k} -> {v}");
    }

    // Key Iterators iterates over the key of the map, in sorted order
    for k in history.keys() {
        println!("{k}");
    }

    // Value Iterators iterates over the values of the map, in order by key
    for v in history.values() {
        println!("{v}");
    }
    
    // HashSet<T> and BTreeSet<T>

    // Sets are collections of values arranges for fast membership testing:
    // let b1 = large_vector.contains(&"needle"); // slow, checks every elements
    // let b2 = large_hash_set.contains(&"needle"); // fast, hash look

    // A set never contains multiple copies of the same value.
    // Maps and sets have different methods, but behind the scenes, a set is like a map
    // with only keys, than key-values pairs. In fact, Rust's two set types, HashSet<T>
    // and BTreeSet<T> are implemented as thin wrappers around HashMap<T, ()> and
    // BTreeMap<T, ()>.

    use std::collections::HashSet;

    let mut books = HashSet::new();

    books.insert("A Dance With Dragons");
    books.insert("To Kill a Mockingbird");
    books.insert("The Odyssey");
    books.insert("The Great Gatsby");

    // Check for a specific one.
    if !books.contains("The Winds of Winter") {
        println!("We have {} books, but The Winds of Winter ain't one.",
            books.len());
    }

    // Remove a book.
    books.remove("The Odyssey");

    // Iterate over everything.
    for book in &books {
        println!("{book}");
    } 
}
