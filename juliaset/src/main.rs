use image::ImageBuffer;
use image::Pixel;
use image::Rgb;
use num::Complex;
use std::env;
use std::process;
use std::str::FromStr;
use rayon::iter::ParallelIterator;

/// Parse the string `s` as a coodinate pair, like `"400x600"` or `"1.0,0.5"`.
///
/// Specifically, `s` should have the form <left><sep><right>, where <sep> is the character given
/// by the `seperator` argument, and <left> and <right> are both strings that can be parsed by
/// `T:from_str`. `seperator` must be an ASCII character.
///
/// If `s` has the proper from, return `Some(x,y)>`. If it doesn't parse correctly, return `None`.
fn parse_pair<T: FromStr>(s: &str, separator: char) -> Option<(T, T)> {
    match s.find(separator) {
        None => None,
        Some(index) => match (T::from_str(&s[..index]), T::from_str(&s[index + 1..])) {
            (Ok(l), Ok(r)) => Some((l, r)),
            _ => None,
        },
    }
}

/// Parse a pair of floating-point numbers separated by a comma as a complex number.
fn parse_complex(s: &str) -> Option<Complex<f64>> {
    match parse_pair(s, ',') {
        Some((re, im)) => Some(Complex { re, im }),
        None => None,
    }
}

fn pixel_to_point(bounds: (usize, usize), pixel: (u32, u32)) -> Complex<f64> {
    Complex {
        re: 3.0 * (pixel.0 as f64 - 0.5 * bounds.0 as f64) / bounds.0 as f64,
        im: 2.0 * (pixel.1 as f64 - 0.5 * bounds.1 as f64) / bounds.1 as f64,
    }
}

fn escape_time(p: Complex<f64>, c: Complex<f64>, limit: usize) -> Option<usize> {
    let mut z = p;

    for i in 0..limit {
        if z.norm_sqr() > 4.0 {
            return Some(i);
        }
        z = z * z + c;
    }
    None
}

/* Parallal rendering using "rayon" */
fn par_render(img: &mut ImageBuffer<Rgb<u8>, Vec<u8>>, bounds: (usize, usize), c: Complex<f64>, limit: usize) { 
    img.par_enumerate_pixels_mut().for_each(|(x, y, pixel)| {
        let point = pixel_to_point(bounds, (x, y));
        let count = escape_time(point, c, limit).unwrap_or(0);
        let color = [
            ((limit - count) << 3) as u8,
            ((limit - count) << 5) as u8,
            ((limit - count) << 4) as u8,
        ];
        *pixel = *Rgb::from_slice(&color);
    });
}

/* Sequential rendering using Iterator on the ImageBuffer directly */
fn render(img: &mut ImageBuffer<Rgb<u8>, Vec<u8>>, bounds: (usize, usize), c: Complex<f64>, limit: usize) {
    for (x, y, pixel) in img.enumerate_pixels_mut() {
        let point = pixel_to_point(bounds, (x, y));
        let count = escape_time(point, c, limit).unwrap_or(0);
        let color = [
            ((limit - count) << 3) as u8,
            ((limit - count) << 5) as u8,
            ((limit - count) << 4) as u8,
        ];
        *pixel = *Rgb::from_slice(&color);
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 5 {
        eprintln!("Usage: {} FILE PIXELS C ITERATION", args[0]);
        eprintln!("Example: {} julia.png 1920x1080 -0.9,0.27015 110", args[0]);
        eprintln!("Example: {} julia.png 1920x1080 -0.7,0.27015 300", args[0]);
        process::exit(1);
    }
    let bounds = parse_pair::<usize>(&args[2], 'x').expect("error parsing image dimensions");
    let c = parse_complex(&args[3]).expect("error parsing complex c");
    let iteration = usize::from_str(&args[4]).expect("error parsing iteration");
    let mut image = ImageBuffer::new(bounds.0 as u32, bounds.1 as u32);

    render(&mut image, bounds, c, iteration);
    image.save(&args[1]).expect("Failed to generate output file");
}
