use std::rc::{Rc, Weak};

#[derive(Default)]
pub struct DoublyLinkedList<T> {
    head: Option<Rc<Node<T>>>,
    tail: Option<Rc<Node<T>>>,
}

pub struct Iter<'a, T> {
    head: Option<&'a Node<T>>,
}

struct Node<T> {
    element: T,
    prev: Option<Weak<Node<T>>>,
    next: Option<Rc<Node<T>>>,
}

impl<T: Copy> DoublyLinkedList<T> {
    pub fn new() -> Self {
        Self { head: None, tail: None }
    }
    pub fn push_front(&mut self, value: T) {
        let mut node = Some(Rc::new(Node { element: value, prev: None, next: None }));

        match self.head {
            Some(ref mut head) => {
                unsafe {
                    Rc::get_mut_unchecked(head).prev = node.as_ref().map(Rc::downgrade);
                    Rc::get_mut_unchecked(node.as_mut().unwrap()).next = self.head.as_ref().map(Rc::clone);
                };
            }
            None => self.tail = node.as_ref().map(Rc::clone),
        }
        self.head = node;
    }
    pub fn pop_front(&mut self) -> Option<T> {
        match self.head.take() {
            Some(mut head) => {
                self.head = head.next.as_ref().map(Rc::clone);
                unsafe { Rc::get_mut_unchecked(&mut head).prev = None }
                Some(head.element)
            }
            None => None,
        }
    }
    pub fn push_back(&mut self, value: T) {
        let mut node = Some(Rc::new(Node { element: value, prev: None, next: None }));

        match self.tail {
            Some(ref mut tail) => {
                unsafe {
                    Rc::get_mut_unchecked(tail).next = node.as_ref().map(Rc::clone);
                    Rc::get_mut_unchecked(node.as_mut().unwrap()).prev = self.tail.as_ref().map(Rc::downgrade);
                };
            }
            None => self.head = node.as_ref().map(Rc::clone),
        }
        self.tail = node;
    }
    pub fn pop_back(&mut self) -> Option<T> {
        match self.tail.take() {
            Some(mut tail) => {
                self.tail = match tail.prev {
                    Some(ref prev) => prev.upgrade(),
                    None => None
                };
                unsafe { Rc::get_mut_unchecked(&mut tail).next = None }
                Some(tail.element)
            }
            None => None,
        }
    }
    pub fn iter(&self) -> Iter<'_, T> {
        Iter {
            head: match self.head {
                Some(ref n) => Some(n.as_ref()),
                None => None,
            },
        }
    }
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<&'a T> {
        match self.head {
            Some(n) => {
                let val = &n.element;
                self.head = match n.next {
                    Some(ref b) => Some(b.as_ref()),
                    None => None,
                };
                Some(val)
            }
            None => None,
        }
    }
}
