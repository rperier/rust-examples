pub struct HeapLessSimplyLinkedList<'e, T, const N: usize> {
    head: *const Node<'e, T>,
    buffer: [Option<Node<'e, T>>; N],
    len: usize,
}

pub struct Iter<'e, T> {
    head: Option<&'e Node<'e, T>>,
}

struct Node<'e, T> {
    element: T,
    next: Option<&'e Node<'e, T>>,
}

impl<'e, T: Copy, const N: usize> HeapLessSimplyLinkedList<'e, T, N> {
    const ARRAY_REPEAT_VALUE: Option<Node<'e, T>> = None;

    fn head_ref(&self) -> Option<&'e Node<'e, T>> {
        if self.head.is_null() {
            None
        } else {
            unsafe { Some(&*self.head) }
        }
    }
    pub fn new() -> Self {
        Self { head: std::ptr::null(), buffer: [Self::ARRAY_REPEAT_VALUE; N], len: 0 }
    }
    pub fn push(&mut self, value: T) -> Result<(), T> {
        if self.len < N {
            self.buffer[self.len] = Some(Node { element: value, next: self.head_ref() });
            self.head = std::ptr::from_ref(self.buffer[self.len].as_ref().unwrap());
            self.len += 1;
            Ok(())
        } else {
            Err(value)
        }
    }
    pub fn pop(&mut self) -> Option<T> {
        match self.head_ref() {
            Some(n) => {
                self.head = match n.next {
                    Some(m) => std::ptr::from_ref(m),
                    None => std::ptr::null(),
                };
                let val = n.element;
                self.buffer[self.len - 1].take();
                self.len -= 1;
                Some(val)
            }
            None => None,
        }
    }

    pub fn iter(&self) -> Iter<'_, T> {
        Iter { head: self.head_ref() }
    }
}

impl<'e, T: Copy, const N: usize> Default for HeapLessSimplyLinkedList<'e, T, N> {
    fn default() -> Self {
        Self::new()
    }
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<&'a T> {
        match self.head {
            Some(n) => {
                let val = &n.element;
                self.head = n.next;
                Some(val)
            }
            None => None,
        }
    }
}
