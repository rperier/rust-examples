 #![feature(get_mut_unchecked)]
pub mod simplylinkedlist;
pub mod heaplesssimplylinkedlist;
pub mod doublylinkedlist;
pub use simplylinkedlist::SimplyLinkedList;
pub use heaplesssimplylinkedlist::HeapLessSimplyLinkedList;
pub use doublylinkedlist::DoublyLinkedList;
