#[derive(Default)]
pub struct SimplyLinkedList<T> {
    head: Option<Box<Node<T>>>,
}

pub struct Iter<'a, T> {
    head: Option<&'a Node<T>>,
}

struct Node<T> {
    element: T,
    next: Option<Box<Node<T>>>,
}

impl<T> SimplyLinkedList<T> {
    pub fn new() -> Self {
        Self { head: None }
    }
    pub fn push(&mut self, value: T) {
        let node = Some(Box::new(Node { element: value, next: self.head.take() }));
        self.head = node;
    }
    pub fn pop(&mut self) -> Option<T> {
        match self.head.take() {
            Some(n) => {
                self.head = n.next;
                Some(n.element)
            }
            None => None,
        }
    }
    pub fn iter(&self) -> Iter<'_, T> {
        Iter {
            head: match self.head {
                Some(ref n) => Some(n.as_ref()),
                None => None,
            },
        }
    }
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<&'a T> {
        match self.head {
            Some(n) => {
                let val = &n.element;
                self.head = match n.next {
                    Some(ref b) => Some(b.as_ref()),
                    None => None,
                };
                Some(val)
            }
            None => None,
        }
    }
}
