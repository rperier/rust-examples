use list::DoublyLinkedList;

#[test]
fn new_list_is_empty() {
    let mut l = DoublyLinkedList::<u8>::new();
    assert_eq!(l.pop_front(), None);
}

#[test]
fn push_front_and_pop_front() {
    let mut l = DoublyLinkedList::new();

    l.push_front(1);
    l.push_front(2);
    l.push_front(3);

    assert_eq!(l.pop_front(), Some(3));
    assert_eq!(l.pop_front(), Some(2));
    assert_eq!(l.pop_front(), Some(1));
    assert_eq!(l.pop_front(), None);
}

#[test]
fn push_back_and_pop_front() {
    let mut l = DoublyLinkedList::new();

    l.push_back(1);
    l.push_back(2);
    l.push_back(3);

    assert_eq!(l.pop_front(), Some(1));
    assert_eq!(l.pop_front(), Some(2));
    assert_eq!(l.pop_front(), Some(3));
    assert_eq!(l.pop_front(), None);
}

#[test]
fn push_back_and_pop_back() {
    let mut l = DoublyLinkedList::new();

    l.push_back(1);
    l.push_back(2);
    l.push_back(3);

    assert_eq!(l.pop_back(), Some(3));
    assert_eq!(l.pop_back(), Some(2));
    assert_eq!(l.pop_back(), Some(1));
    assert_eq!(l.pop_back(), None);
}

#[test]
fn push_front_and_pop_back() {

    let mut l = DoublyLinkedList::new();

    l.push_front(1);
    l.push_front(2);
    l.push_front(3);

    assert_eq!(l.pop_back(), Some(1));
    assert_eq!(l.pop_back(), Some(2));
    assert_eq!(l.pop_back(), Some(3));
    assert_eq!(l.pop_back(), None);
}

#[test]
fn list_is_consistent_after_push_front_and_push_back() {
    let mut l = DoublyLinkedList::new();

    l.push_front(3);
    l.push_front(2);
    l.push_front(1);

    l.push_back(4);
    l.push_back(5);
    l.push_back(6);

    assert_eq!(l.pop_front(), Some(1));
    assert_eq!(l.pop_front(), Some(2));
    assert_eq!(l.pop_front(), Some(3));
    assert_eq!(l.pop_front(), Some(4));
    assert_eq!(l.pop_front(), Some(5));
    assert_eq!(l.pop_front(), Some(6));
    assert_eq!(l.pop_front(), None);
}

#[test]
fn iter_do_not_alter_list() {
    let mut l = DoublyLinkedList::new();

    l.push_front(1);
    l.push_front(2);
    l.push_front(3);

    let mut iter = l.iter();
    assert_eq!(iter.next(), Some(&3));
    assert_eq!(iter.next(), Some(&2));
    assert_eq!(iter.next(), Some(&1));
    assert_eq!(iter.next(), None);

    assert_eq!(l.pop_front(), Some(3));
    assert_eq!(l.pop_front(), Some(2));
    assert_eq!(l.pop_front(), Some(1));
}
