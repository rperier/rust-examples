use list::HeapLessSimplyLinkedList;

#[test]
fn new_list_is_empty() {
    let mut l = HeapLessSimplyLinkedList::<u8, 0>::new();
    assert_eq!(l.pop(), None);
}

#[test]
fn push_and_pop() {
    let mut l = HeapLessSimplyLinkedList::<u8, 3>::new();

    assert!(l.push(1).is_ok());
    assert!(l.push(2).is_ok());
    assert!(l.push(3).is_ok());

    assert_eq!(l.pop(), Some(3));
    assert_eq!(l.pop(), Some(2));
    assert_eq!(l.pop(), Some(1));
    assert_eq!(l.pop(), None);
}

#[test]
fn iter_do_not_alter_list() {
    let mut l = HeapLessSimplyLinkedList::<u8, 3>::new();

    assert!(l.push(1).is_ok());
    assert!(l.push(2).is_ok());
    assert!(l.push(3).is_ok());

    let mut iter = l.iter();
    assert_eq!(iter.next(), Some(&3));
    assert_eq!(iter.next(), Some(&2));
    assert_eq!(iter.next(), Some(&1));
    assert_eq!(iter.next(), None);

    assert_eq!(l.pop(), Some(3));
    assert_eq!(l.pop(), Some(2));
    assert_eq!(l.pop(), Some(1));
}
