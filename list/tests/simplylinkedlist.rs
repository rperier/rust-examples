use list::SimplyLinkedList;

#[test]
fn new_list_is_empty() {
    let mut l = SimplyLinkedList::<u8>::new();
    assert_eq!(l.pop(), None);
}

#[test]
fn push_and_pop() {
    let mut l = SimplyLinkedList::new();

    l.push(1);
    l.push(2);
    l.push(3);

    assert_eq!(l.pop(), Some(3));
    assert_eq!(l.pop(), Some(2));
    assert_eq!(l.pop(), Some(1));
    assert_eq!(l.pop(), None);
}

#[test]
fn iter_do_not_alter_list() {
    let mut l = SimplyLinkedList::new();

    l.push(1);
    l.push(2);
    l.push(3);

    let mut iter = l.iter();
    assert_eq!(iter.next(), Some(&3));
    assert_eq!(iter.next(), Some(&2));
    assert_eq!(iter.next(), Some(&1));
    assert_eq!(iter.next(), None);

    assert_eq!(l.pop(), Some(3));
    assert_eq!(l.pop(), Some(2));
    assert_eq!(l.pop(), Some(1));
}
