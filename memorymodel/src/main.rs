fn modify(mut v: Vec<String>) -> Vec<String> {
    for s in &mut v {
        s.push('!');
    }
    v
}

fn modify_slice(v: &mut [String]) {
    for s in v {
        s.push('!');
    }
}

fn main() {
    
    /* Example 0 : no ownership on primitive data types (Copy types) */
    let v = 3;
    let t = v;

    assert_eq!(v, 3);
    assert_eq!(t, 3);
    println!("{v}");
    println!("{t}");

    /* Example 1 */

    /* v moves into t here */
    let v = vec! ["un".to_string(), "deux".to_string(), "trois".to_string() ];
    let t = v;
    assert_eq!(t[0], "un");
    assert_eq!(t[1], "deux");
    assert_eq!(t[2], "trois");
    // cannot display v (because it has moved to t, it is uninitialized now)
    //println!("{}", v[1]);
    
    /* Example 2 */
    /* v moves into the for loop, that's the for loop which takes owership of the vector */
    let v = vec! ["un".to_string(), "deux".to_string(), "trois".to_string() ];

    for mut s in v {
        s.push('!');
        println!("{s}");
    }
    
    /* Example 3: modify a vector from a function (with move owership) */
    /* v moves into the function modify */
    let mut v = vec! ["un".to_string(), "deux".to_string(), "trois".to_string() ];
    v = modify(v); // v is moved here
    assert_eq!(v[0], "un!");
    assert_eq!(v[1], "deux!");
    assert_eq!(v[2], "trois!");
    for s in v {
        println!("{s}");
    }
    
    /* Example 4: same as Example 3 without move ownership, we use Slices */
    let mut v = vec! ["un".to_string(), "deux".to_string(), "trois".to_string() ];
    modify_slice(&mut v);
    assert_eq!(v[0], "un!");
    assert_eq!(v[1], "deux!");
    assert_eq!(v[2], "trois!");
    for s in v {
        println!("{s}");
    }
}
