use std::ops::Add;
use std::ops::AddAssign;
use std::ops::Neg;
use std::ops::Not;
use std::ops::Shl;
use std::cmp::{Ordering, PartialOrd};

#[derive(Clone, Copy, PartialEq, Debug)]
struct Complex<T> {
    /// Real portion of the complex number
    re: T,
    /// Imaginary portion of the complex number
    im: T,
}

impl<T> Complex<T> {
    fn new(re: T, im: T) -> Self {
        Self { re, im }
    }
}

impl<T> Add for Complex<T>
where
    T: Add<Output = T>,
{
    type Output = Self;

    fn add(self, rhs: Self) -> Self {
        Self {
            re: self.re + rhs.re,
            im: self.im + rhs.im,
        }
    }
}

impl<T> Add<T> for Complex<T>
where
    T: Add<Output = T>,
{
    type Output = Self;
    fn add(self, rhs: T) -> Self {
        Self {
            re: self.re + rhs,
            im: self.im,
        }
    }
}

impl<T> Neg for Complex<T>
where
    T: Neg<Output = T>,
{
    type Output = Self;

    fn neg(self) -> Self {
        Self {
            re: -self.re,
            im: -self.im,
        }
    }
}

impl<T> Not for Complex<T>
where
    T: Not<Output = T>,
{
    type Output = Self;

    fn not(self) -> Self {
        Self {
            re: !self.re,
            im: !self.im,
        }
    }
}

impl<T> Shl for Complex<T>
where
    T: Shl<Output = T>,
{
    type Output = Self;

    fn shl(self, rhs: Self) -> Self {
        Self {
            re: self.re << rhs.re,
            im: self.im << rhs.im,
        }
    }
}

impl<T> Shl<T> for Complex<T>
where
    T: Shl<Output = T>,
{
    type Output = Self;
    fn shl(self, rhs: T) -> Self {
        Self {
            re: self.re << rhs,
            im: self.im,
        }
    }
}

impl<T> AddAssign for Complex<T>
where
    T: AddAssign<T>,
{
    fn add_assign(&mut self, rhs: Self) {
        self.re += rhs.re;
        self.im += rhs.im;
    }
}

#[derive(Debug, PartialEq)]
struct Interval<T>
{
    lower: T, // inclusive
    upper: T // exclusive
}

impl<T: PartialOrd> PartialOrd<Interval<T>> for Interval<T> {
    fn partial_cmp(&self, other: &Interval<T>) -> Option<Ordering> {
      if self == other {
          Some(Ordering::Equal)
      } else if self.lower >= other.upper {
          Some(Ordering::Greater)
      } else if self.upper <= other.lower {
          Some(Ordering::Less)
      } else {
          None
      }
    }
}

// For a simple a first introduction and use of normal Trait, see the list crate
// (implementing Iterator Trait and use type-associated trait)
fn main() {
    let c = Complex::new(1.0, 1.0);
    let d = Complex::new(1.0, 0.0);

    assert_eq!(c.re, 1.0);
    assert_eq!(c.im, 1.0);

    /* Basic binary operators */

    // First Trait implementation
    assert_eq!(c + d, Complex::new(2.0, 1.0));

    // Second Trait implementation
    assert_eq!(c + 2.0, Complex::new(3.0, 1.0));

    /* Unary operators */

    // Neg operator
    assert_eq!(-c, Complex::new(-1.0, -1.0));
    assert_eq!(-c, -c);
    assert_eq!(--c, c);

    // Not operator
    let e = Complex::new(1, 1); // Operator Not is not supported for floating types
    assert_eq!(!e, Complex::new(!1, !1));
    assert_eq!(!e, !Complex::new(1, 1));
    assert_eq!(!e, !e);
    assert_eq!(!!e, e);

    /* Other binary operators */
    assert_eq!(e << Complex::new(1, 0), Complex::new(2, 1));
    assert_eq!(e << 1, Complex::new(2, 1));

    /* Compound Assignment Operators */
    let mut f = c;
    f += d;
    assert_eq!(f, Complex::new(2.0, 1.0));

    /* Ordered Comparisons */
    assert!(Interval { lower: 10, upper: 20} < Interval { lower: 20, upper: 40 });
    assert!(Interval { lower: 7, upper: 8} >= Interval { lower: 0, upper: 1 });
    assert!(Interval { lower: 7, upper: 8} <= Interval { lower: 7, upper: 8 });

    // Overlapping intervals are not ordered with respect to each other
    let left = Interval { lower: 10, upper: 30};
    let right = Interval { lower: 20, upper: 40};
    assert!(!(left < right));
    assert!(!(left >= right));
}
