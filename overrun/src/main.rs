fn main() {
    let array: [u32; 5] = [0, 0, 0, 0, 0];

    println!("Let's overflow the buffer!");

    for index in 0..8 {
        println!("Index {}: {}", index, array[index]);
    }
}
