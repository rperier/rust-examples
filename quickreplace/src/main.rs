use anyhow::{Context, Result};
use colored::*;
use regex::Regex;
use std::{env, fs};

struct Arguments {
    target: String,
    replacement: String,
    filename: String,
    output: String,
}

fn print_usage() {
    eprintln!("quickreplace - change occurences of one string into another");
    eprintln!("{} {} {}", "Usage:".green(), "quickreplace".cyan().bold(),
              "<target> <replacement> <INPUT> <OUTPUT>".cyan());
}

fn parse_args() -> Arguments {
    let args: Vec<String> = env::args().skip(1).collect();

    if args.len() != 4 {
        print_usage();
        eprintln!("{} wrong number of arguments: expected 4, got {}", "Error:".red().bold(),
                  args.len());
        std::process::exit(1);
    }

    Arguments {
        target: args[0].clone(),
        replacement: args[1].clone(),
        filename: args[2].clone(),
        output: args[3].clone(),
    }
}

fn replace(target: &str, replacement: &str, text: &str) -> Result<String, regex::Error> {
    let regex = Regex::new(target)?;
    Ok(regex.replace_all(text, replacement).to_string())
}

fn main() -> Result<()> {
    let args = parse_args();

    let data = fs::read_to_string(&args.filename).with_context(|| format!("Failed to read {}", args.filename))?;
    let replaced_data = replace(&args.target, &args.replacement, &data)?;
    fs::write(&args.output, replaced_data).with_context(|| format!("Failed to write {}", args.output))?;
    Ok(())
}
