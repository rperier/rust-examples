use anyhow::{Context, Result};
use getopts::Options;
use std::env;
use std::fs::read;
use std::io;
use std::process;

fn cat<F>(reader: F, number_nonblank: bool, show_ends: bool, number: bool, squeeze_blank: bool, show_tabs: bool) -> Result<()>
where
    F: Fn(&mut Vec<u8>) -> Result<usize>,
{
    let mut count = 0;

    loop {
        let mut data = Vec::new();
        let size = reader(&mut data)?;
        let mut buffer = String::from_utf8(data)?;

        if number_nonblank || number {
            buffer = buffer
                .lines()
                .map(|x| {
                    if number_nonblank {
                        if !x.is_empty() {
                            count += 1;
                            count.to_string() + " " + x + "\n"
                        } else {
                            x.to_owned() + "\n"
                        }
                    } else {
                        count += 1;
                        count.to_string() + " " + x + "\n"
                    }
                })
                .collect();
        }
        if squeeze_blank {
            let mut prev = ' ';
            let mut pprev = ' ';
            buffer.retain(|c| {
                let result = c != '\n' || prev != '\n' || pprev != '\n';
                pprev = prev;
                prev = c;
                result
            });
        }
        if show_ends {
            buffer = buffer.replace('\n', "$\n");
        }
        if show_tabs {
            buffer = buffer.replace('\t', "^I");
        }
        print!("{buffer}");

        if size == 0 {
            break;
        }
    }
    Ok(())
}

fn usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [OPTIONS]... [FILE]...", program);
    println!("{}", opts.usage(&brief));
    process::exit(1);
}

fn stdin_reader(output: &mut Vec<u8>) -> Result<usize> {
    let mut buf = String::new();
    let s = io::stdin().read_line(&mut buf).context("Failed to read stdin")?;

    *output = buf.into_bytes();
    Ok(s)
}

fn main() -> Result<()> {
    let args: Vec<_> = env::args().collect();
    let mut opts = Options::new();

    opts.optflag("b", "number-nonblank", "number nonempty output lines");
    opts.optflag("E", "show-ends", "display $ at end of each line");
    opts.optflag("n", "number", "number all output lines");
    opts.optflag("s", "squeeze-blank", "suppress repeated empty output lines");
    opts.optflag("T", "show-tabs", "display TAB characters as ^I");
    opts.optflag("h", "help", "display this help and exit");

    let matches = opts.parse(&args[1..])?;
    if matches.opt_present("h") {
        usage(&args[0], opts);
    }

    if matches.free.is_empty() {
        cat(stdin_reader, matches.opt_present("b"), matches.opt_present("E"),matches.opt_present("n"),
            matches.opt_present("s"), matches.opt_present("T"))?;
    }

    for file in &matches.free {
        cat(|x| {
                *x = read(file).with_context(|| format!("Failed to read {file}"))?;
                Ok(0)
            }, matches.opt_present("b"), matches.opt_present("E"), matches.opt_present("n"),
            matches.opt_present("s"), matches.opt_present("T"))?;
    }
    Ok(())
}
