use std::collections::HashMap;

type Table = HashMap<String, Vec<String>>;

struct Anime {
    name: &'static str,
    bechdel_pass: bool
}

struct Point {
    x: i32,
    y: i32
}

fn show(table: &Table) {
    for (artists, works) in table {
        println!("Works by {}:", artists);
        for work in works {
            println!("  {}", work);
        }
    }
}

fn sort_work(table: &mut Table) {
    for works in table.values_mut() {
        works.sort();
    }
}

fn factorial(n: usize) -> usize {
    (1..n+1).product()
}

fn smallest(v: &[u32]) -> &u32 {
    let mut s = &v[0];
    for r in &v[1..] {
        if r < s {
            s = r;
        }
    }
    s
}

fn main() {
    /* Example 1 : By values and by references */
    let mut table = Table::new();

    table.insert("Tool".to_string(), vec!["Undertow".to_string(), "AEnima".to_string(), "Lateralus".to_string(), "10,000 Days".to_string(), "Fear Inoculum".to_string()]);
    table.insert("ACDC".to_string(), vec!["Ballbreaker".to_string(), "Powerage".to_string(), "Black Ice".to_string(), "Let There Be Rock".to_string(), "For Those About To Rock We Salute You".to_string()]);

    // Normally sort_work and show should move table and then table should be uninitialiazed
    // We pass it by reference so it can be modified, and then display and then asserted
    sort_work(&mut table);
    show(&table);
    assert_eq!(table["Tool"][0], "10,000 Days");

    /* Example 2 : Working with references */
    let x = 10;
    let r = &x;         // r is a shared reference to x
    assert!(*r == 10);  // explicitly dereference r

    let mut y = 32;
    let m = &mut y;     // &mut y is a mutable reference to y
    *m += 32;           // explicitly deference m to see y's value
    assert!(*m == 64);
    assert!(y == 64);

    let aria = Anime { name : "Aria: The animation", bechdel_pass: true };
    let anime_ref = &aria;
    // operator . implicitly dereference its left operand
    assert_eq!(anime_ref.name, "Aria: The animation");
    assert!(anime_ref.bechdel_pass);
    // Equivalent to the above, but with dereference written out
    assert_eq!((*anime_ref).name, "Aria: The animation");
    assert!((*anime_ref).bechdel_pass);

    let mut v = vec![1973, 1968];
    v.sort();           // implicitly borrow a mutable reference to v, for a method call
    (&mut v).sort();    // same but more verbose
    assert_eq!(v[0], 1968);

    /* Example 3 : Assigning references */
    let b = match std::env::var("B") {
        Ok(v) => v,
        _ => "".to_string(),
    };
    let x = 10;
    let y = 20;
    let mut r = &x;

    // Reference to a value might be changed my assignement (different from c++ where it is only at
    // initialization)
    if !b.is_empty() {
        r = &y;
    }
    assert!(*r == 10 || *r == 20);
    println!("{r}");

    /* Example 4: References references */
    let point = Point { x: 1000, y: 729};
    let r = &point;
    let rr = &r;
    let rrr = &rr;

    // The operator . follows as many references as it takes to find it target
    assert_eq!(r.x, 1000);
    assert_eq!(r.y, 729);
    assert_eq!(rr.x, 1000);
    assert_eq!(rr.y, 729);
    assert_eq!(rrr.x, 1000);
    assert_eq!(rrr.y, 729);

    /* Exemple 5 : Comparing references */
    let x = 10;
    let y = 10;
    let rx = &x;
    let ry = &y;
    let rrx = &rx;
    let rry = &ry;

    // The operator == or <= follow all references and perform the comparison on the final target x
    // and y
    assert!(rrx <= rry);    // 10 <= 10
    assert!(rrx == rry);    // 10 == 10
    assert!(rx == ry);      // 10 == 10
    assert!(!std::ptr::eq(rx, ry)); // they are at different addresses

    //assert!(rx == rrx); // not working, because it must have the same type
    assert!(rx == *rrx); // okay

    /* Example 6: Borrowing references to arbitrary expressions */
    let r = &factorial(6);
    // Arithmetic operator can see throught one level of reference
    assert_eq!(r + &1009, 1729);
    assert_eq!(r + 1009, 1729);
    println!("{r}");

    /* Example 7 : Returning references */
    let a = [9, 4, 1, 0, 1, 4, 9];
    let r = smallest(&a);
    assert_eq!(*r, 0);

    let v = vec![9, 4, 1, 0, 1, 4, 9];
    let s = smallest(&v);
    assert_eq!(*s, 0);
}
