use anyhow::Result;
use getopts::Options;
use std::env;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};
use std::sync::mpsc;
use std::thread;

mod index;
mod tmp;
mod read;
mod write;
mod merge;

use crate::index::InMemoryIndex;
use crate::tmp::TmpDir;
use crate::write::write_index_to_tmp_file;
use crate::merge::FileMerge;

fn start_file_reader_thread(documents: Vec<PathBuf>) -> (mpsc::Receiver<String>, thread::JoinHandle<io::Result<()>>) {
    let (sender, receiver) = mpsc::channel();

    let handle = thread::spawn(move || {
        for filename in documents {
            let text = fs::read_to_string(filename)?;
            if sender.send(text).is_err() {
                break;
            }
        }
        Ok(())
    });
    (receiver, handle)
}

fn start_file_indexing_thread(texts: mpsc::Receiver<String>) -> (mpsc::Receiver<InMemoryIndex>, thread::JoinHandle<()>) {
    let (sender, receiver) = mpsc::channel();

    let handle = thread::spawn(move || {
        for (doc_id, text) in texts.into_iter().enumerate() {
            let index = InMemoryIndex::from_single_document(doc_id, text);
            if sender.send(index).is_err() {
                break;
            }
        }
    });
    (receiver, handle)
}

fn start_in_memory_merge_thread(files_indexes: mpsc::Receiver<InMemoryIndex>) -> (mpsc::Receiver<InMemoryIndex>, thread::JoinHandle<()>) {
    let (sender, receiver) = mpsc::channel();

    let handle = thread::spawn(move || {
        let mut accumulated_index = InMemoryIndex::new();
        for fi in files_indexes {
            accumulated_index.merge(fi);
            if accumulated_index.is_large() {
                if sender.send(accumulated_index).is_err() {
                    return;
                }
                accumulated_index = InMemoryIndex::new();
            }
        }
        if !accumulated_index.is_empty() {
            let _ = sender.send(accumulated_index);
        }
    });
    (receiver, handle)
}

fn start_index_writer_thread(big_indexes: mpsc::Receiver<InMemoryIndex>, output_dir: &Path) -> (mpsc::Receiver<PathBuf>, thread::JoinHandle<io::Result<()>>) {
    let (sender, receiver) = mpsc::channel();

    let mut tmp_dir = TmpDir::new(output_dir);
    let handle = thread::spawn(move || {
        for index in big_indexes {
            let file = write_index_to_tmp_file(index, &mut tmp_dir)?;
            if sender.send(file).is_err() {
                break;
            }
        }
        Ok(())
    });
    (receiver, handle)
}

fn merge_index_files(files: mpsc::Receiver<PathBuf>, output_dir: &Path) -> io::Result<()> {
    let mut merge = FileMerge::new(output_dir);
    for file in files {
        merge.add_file(file)?;
    }
    merge.finish()
}

fn run_pipeline(documents: Vec<PathBuf>, output_dir: PathBuf) -> io::Result<()> {
    let (texts, h1) = start_file_reader_thread(documents);
    let (indexes, h2) = start_file_indexing_thread(texts);
    let (big_index, h3) = start_in_memory_merge_thread(indexes);
    let (files, h4) = start_index_writer_thread(big_index, &output_dir);
    let result = merge_index_files(files, &output_dir);

    let r1 = h1.join().unwrap();
    h2.join().unwrap();
    h3.join().unwrap();
    let r4 = h4.join().unwrap();

    r1?;
    r4?;
    result
}

fn main() -> Result<()> {
    const EXTENSION: &str = "h";
    let args: Vec<_> = env::args().collect();
    let mut opts = Options::new();

    opts.reqopt("f","filenames" ,"Names of files/directories to index.For directories, all .txt files immediately under the directory are indexed" , "FILE");

    let matches = opts.parse(&args[1..])?;
    let mut documents = vec![];

    for f in fs::read_dir(&matches.opt_strs("f")[0])? {
        let f = f?;
        let p = f.path();
        if !f.file_type()?.is_file() {
            continue;
        }
        let Some(ext) = p.extension() else {
            anyhow::bail!("Unable to get extension for file {}", p.display());
        };
        if ext == EXTENSION {
            documents.push(p);
        }
    }

    run_pipeline(documents, PathBuf::from("."))?;
    Ok(())
}
