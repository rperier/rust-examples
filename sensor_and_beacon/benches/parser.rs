use criterion::{black_box, criterion_group, criterion_main, Criterion};
use sensor_and_beacon::{parse_line, parse_line_nom, parse_line_regex};

fn bench_parse_nom(c: &mut Criterion) {
    c.bench_function("parse_nom", |b| { b.iter(|| { black_box(
        parse_line_nom(black_box("Sensor at x=12, y=14: closest beacon is at x=10, y=16"))
                .unwrap()); })
    });
}

fn bench_parse_line(c: &mut Criterion) {
    c.bench_function("parse_str", |b| {
        b.iter(|| {
            black_box(
                parse_line(black_box(
                    "Sensor at x=12, y=14: closest beacon is at x=10, y=16",
                ))
                .unwrap(),
            );
        })
    });
}

fn bench_parse_regex(c: &mut Criterion) {
    c.bench_function("parse_regex", |b| {
        b.iter(|| {
            black_box(
                parse_line_regex(black_box(
                    "Sensor at x=12, y=14: closest beacon is at x=10, y=16",
                ))
                .unwrap(),
            );
        })
    });
}

criterion_group!(benches, bench_parse_nom, bench_parse_line, bench_parse_regex);
criterion_main!(benches);
