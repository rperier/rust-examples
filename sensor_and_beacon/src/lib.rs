use anyhow::Result;
use regex::{Error, Regex};
use std::sync::LazyLock;
use nom::{IResult, Parser, bytes::complete::tag, sequence::{preceded, separated_pair}};

#[derive(Debug, PartialEq)]
pub struct Pos {
    pub x: i16,
    pub y: i16,
}

// The lib.rs module is required because of Criterion, with Bencher from rust nightly everything might be contained
// into the same module and crate. See https://github.com/bheisler/criterion.rs/issues/353
static RE: LazyLock<Regex> = LazyLock::new(|| {
    Regex::new(r#"x=(?<x1>[-]?\d+), y=(?<y1>[-]?\d+): closest beacon is at x=(?<x2>[-]?\d+), y=(?<y2>[-]?\d+)"#).unwrap()
});

pub fn parse_line_regex(line: &str) -> Result<(Pos, Pos)> {
    let c = RE.captures(line).ok_or(Error::Syntax(String::from("Capture does not match")))?;

    Ok((Pos { x: c["x1"].parse()?, y: c["y1"].parse()? }, Pos { x: c["x2"].parse()?, y: c["y2"].parse()? }))
}

pub fn parse_line(line: &str) -> Result<(Pos, Pos)> {
    let abs: Result<Vec<_>, _> = line.match_indices("x=").map(|t| {
        line[t.0 + 2..].split(',').nth(0)
        .ok_or(Error::Syntax("x coords syntax is wrong".to_string()))
    }).collect();
    let ords: Result<Vec<_>, _> = line.match_indices("y=").map(|t| {
        line[t.0 + 2..].split(|c| c == ':' || c == '\n').nth(0)
        .ok_or(Error::Syntax("y coords syntax is wrong".to_string()))
    }).collect();
    let (x, y) = (abs?, ords?);

    Ok(( Pos { x: x[0].parse()?, y: y[0].parse()? }, Pos { x: x[1].parse()?, y: y[1].parse()? }))
}

fn coords(line: &str) -> IResult<&str, (i16, i16)> {
    separated_pair(
        preceded(tag("x="), nom::character::complete::i16),
        tag(", "),
        preceded(tag("y="), nom::character::complete::i16),
    )(line)
}

pub fn parse_line_nom(line: &str) -> Result<(Pos, Pos)> {
    let output = preceded(tag("Sensor at "),
        separated_pair(
            coords.map(|(x, y)| Pos { x, y}),
            tag(": closest beacon is at "),
            coords.map(|(x, y)| Pos { x, y })
    ))(line).map_err(|_| Error::Syntax("y coords syntax is wrong".to_string()))?;

    Ok((output.1.0, output.1.1))
}
