use anyhow::Result;
use sensor_and_beacon::parse_line_nom;
use std::fs::read_to_string;
use std::{env, process};

fn main() -> Result<()> {
    let args: Vec<_> = env::args().skip(1).collect();
    if args.len() != 1 {
        process::exit(1);
    }

    let content = read_to_string(&args[0])?;
    let points: Result<Vec<_>> = content.lines().map(parse_line_nom).collect();

    for (s, b) in points? {
        println!("Sensor ({}, {}) , Beacon ({}, {})", s.x, s.y, b.x, b.y);
    }

    Ok(())
}
