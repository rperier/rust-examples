use std::cell::{Cell, RefCell};
use std::fs::{File, remove_file};
use std::io::Write;

struct GrayscaleMap {
    pixels: Vec<u8>,
    size: (usize, usize),
}

/// In this game, brooms are monsters.
struct Broom {
    name: String,
    height: u32,
    health: u32,
    position: (f32, f32, f32),
    intent: BroomIntent,
}

/// Two possible alternatives for what a `Broom` could be working on.
#[derive(Copy, Clone)]
enum BroomIntent {
    FetchWatcher,
    DumpWather,
}

/// A pair of an image boundary
struct Bounds(usize, usize);

impl Bounds {
    fn size(&self) -> usize {
        self.0 * self.1
    }
}

/// A first-in, first-out queue of characters.
struct Queue {
    older: Vec<char>,  // older elements, eldest last.
    younger: Vec<char>, // younger elements, youngest last.
}

// Functions defined in an impl block are called associated funtions, since they're associated
// with a specific type. The opposite of an associated function is a free function, one that is
// not defined as an impl block's item.
impl Queue {
    /// Create a new Queue.
    fn new() -> Queue {
        /* An impl block for a given type can also define functions that don't take self as an
         * argument at all. These are still associated functions, since they're in an impl block,
         * but they're not methods. Since they don't take a self argument. To distinguish them from
         * methods, we call them type-associated functions. */
        Queue { older: Vec::new(), younger: Vec::new() }
    }
    /// Test if the queue is empty.
    fn is_empty(&self) -> bool {
        // If a method does not need to modify its self, then you can define it to take a shared
        // reference instead.
        self.older.is_empty() && self.younger.is_empty()
    }

    /// Push a character onto the back of the queue.
    fn push(&mut self, c: char) {
        self.younger.push(c);
    }

    /// Pop a character off the front of the queue. Return `Some(c)` if there was a character to
    /// pop or `None` if the queue was empty.
    fn pop(&mut self) -> Option<char> {
        if self.older.is_empty() {
            if self.younger.is_empty() {
                return None;
            }

            // Bring the elements in younger over to older, and put them in the promised order.
            use std::mem::swap;
            swap(&mut self.older, &mut self.younger);
            self.older.reverse();
        }

        // Now older is guaranteed to have something. Vec's pop method already returns an Option, so
        // we're set.
        self.older.pop()
    }

    /// Split the queue into two Vec
    fn split(self) -> (Vec<char>, Vec<char>) {
        // If a method wants to take ownership of self, it can take self by value
        (self.older, self.younger)
    }
}

/// A first-in, first-out queue of any type.
/* We use the name `QueueGen` to be able to use both types in this program.
 * This is purely for conveniance, otherwise we would had defined the Queue<T> generic
 * struct directly */
struct QueueGen<T> {
    older: Vec<T>,  // older elements, eldest last.
    younger: Vec<T>, // younger elements, youngest last.
}

impl<T> QueueGen<T> {
    /*
    /// Create a new Queue.
    fn new() -> QueueGen<T> { // Self is a shorthand for QueueGen<T>
        /* An impl block for a given type can also define functions that don't take self as an
         * argument at all. These are still associated functions, since they're in an impl block,
         * but they're not methods. Since they don't take a self argument. To distinguish them from
         * methods, we call them type-associated functions. */
        QueueGen { older: Vec::new(), younger: Vec::new() }
    }
    */
    
    /// Create a new Queue.
    fn new() -> Self { // This is the simplified version with shorthand
        Self { older: Vec::new(), younger: Vec::new() }
    }
    /// Test if the queue is empty.
    fn is_empty(&self) -> bool {
        // If a method does not need to modify its self, then you can define it to take a shared
        // reference instead.
        self.older.is_empty() && self.younger.is_empty()
    }

    /// Push a character onto the back of the queue.
    fn push(&mut self, t: T) {
        self.younger.push(t);
    }

    /// Pop a character off the front of the queue. Return `Some(c)` if there was a character to
    /// pop or `None` if the queue was empty.
    fn pop(&mut self) -> Option<T> {
        if self.older.is_empty() {
            if self.younger.is_empty() {
                return None;
            }

            // Bring the elements in younger over to older, and put them in the promised order.
            use std::mem::swap;
            swap(&mut self.older, &mut self.younger);
            self.older.reverse();
        }

        // Now older is guaranteed to have something. Vec's pop method already returns an Option, so
        // we're set.
        self.older.pop()
    }

    /// Split the queue into two Vec
    fn split(self) -> (Vec<T>, Vec<T>) {
        // If a method wants to take ownership of self, it can take self by value
        (self.older, self.younger)
    }
}

// This gives QueueGen<f64> a sum method, available on no other kind of QueueGen.
impl QueueGen<f64> {
    fn sum(&self) -> f64 {
        self.older.iter().sum::<f64>() + self.younger.iter().sum::<f64>()
    }
}

struct Vector2 {
    x: f32,
    y: f32
}

impl Vector2 {
    const ZERO: Vector2 = Vector2 { x: 0.0, y: 0.0 };
    const UNIT: Vector2 = Vector2 { x: 1.0, y: 0.0 };
}

/// A structure that might hold references to the greatest and least elements of some slice
struct Extrema<'elt> {
    greatest: &'elt i32,
    least: &'elt i32
}

/*
 * Because it's so common for the return type to use the same lifetime as an argument, Rust let us
 * omit the lifetimes when there's one obvious candidate. We could also have written find_extrama's
 * signature like this, with no change in meaning:
 * fn find_extrama(slice: &[i32]) -> Extrema {
 * ...
 * }
 * */
fn find_extrama<'s>(slice: &'s [i32]) -> Extrema<'s> {
    let mut greatest = &slice[0];
    let mut least = &slice[0];

    for i in 1..slice.len() {
        if slice[i] < *least {
            least = &slice[i];
        }
        if slice[i] > *greatest {
            greatest = &slice[i];
        }
    }

    Extrema { greatest, least } 
}

/// A polynomial of degree N - 1.
struct Polynomial<const N: usize> {
    /// The coefficients of the polynomial.
    ///
    /// For a polynomial a + bx + cx² + ... + zx^(n-1)
    /// the `i`th element is the coefficient of x^i.
    coefficients: [f64; N]
}

impl<const N: usize> Polynomial<N> {
    fn new(coefficients: [f64; N]) -> Self {
        Self { coefficients }
    }

    /// Evaluate the polynomial at `x`.
    fn eval(&self, x: f64) -> f64 {
        // Horner's method is numerically stable, efficient, and simple:
        // c0 + x (c1 + x(c2 + x(c3 + ... x(c[n-1] + x c[n]))))
        let mut sum = 0.0;
        for i in (0..N).rev() {
            sum = self.coefficients[i] + x * sum;
        }
        sum
    }
}

struct SpiderRobot {
    species: String,
    web_enabled: bool,
    hardware_error_count: Cell<u32>,
    log_file: RefCell<File>
}

impl SpiderRobot {
    fn new(species: String, web_enabled: bool, log_file: String) -> Self {
        Self {species, web_enabled, hardware_error_count: Cell::new(0), log_file: RefCell::new(File::create_new(log_file).unwrap()) }
    }

    /// Increase the error count by 1.
    fn add_hardware_error(&self) {
        let n = self.hardware_error_count.get();
        self.hardware_error_count.set(n + 1);
    }

    /// True if any hardware errors have been reported.
    fn has_hardware_error(&self) -> bool {
        self.hardware_error_count.get() > 0
    }

    /// Write a line to the log file.
    fn log (&self, message: &str) {
        let mut file = self.log_file.borrow_mut();
        // `writeln!` is like `println!`, but sends
        // output to the given file.
        writeln!(file, "{}", message).unwrap();
    }
}

/// Receive the input Broom by value, taking ownership.
fn chop(b: Broom) -> (Broom, Broom) {
    // Initialize `broom1` mostly from `b`, changing only `height`. Since
    // `String` is not a `Copy`, `broom1` takes ownership of `b`'s name.
    let mut broom1 = Broom { height: b.height / 2, ..b };

    // Initialize `broom2` mostly from `broom1`. Since `String`is not
    // `Copy`, we must clone `name` explicitly.
    let mut broom2 = Broom { name: broom1.name.clone(), ..broom1 };

    // Give each fragment a distinct name.
    broom1.name.push_str(" I");
    broom2.name.push_str(" II");

    (broom1, broom2)
}

fn new_map(size: (usize, usize), pixels: Vec<u8>) -> GrayscaleMap {
    assert_eq!(pixels.len(), size.0 * size.1);

    GrayscaleMap { pixels, size }
}

fn main() {
    /* Example 1: Named-Field Structs*/
    let width = 1024;
    let height = 576;
    /* There are two ways for initializing a Name-Field Struct
     * by explicitly listing its fields by name */
    let image = GrayscaleMap {
        pixels: vec![0; width * height],
        size: (width, height),
    };
    /* Or this way (see the return type) */
    let img = new_map((width, height), vec![0; width * height]);

    assert_eq!(image.size, (width, height));
    assert_eq!(image.pixels.len(), width * height);
    assert_eq!(img.size, image.size);
    assert_eq!(img.pixels.len(), image.pixels.len());

    /* You can also partially initializing a struct with another of the
     * same type */
    let hokey = Broom {
        name: "Hokey".to_string(),
        height: 60,
        health: 100,
        position: (100.0, 200.0, 0.0),
        intent: BroomIntent::FetchWatcher,
    };

    let (hokey1, hokey2) = chop(hokey);
    assert_eq!(hokey1.name, "Hokey I");
    assert_eq!(hokey1.height, 30);
    assert_eq!(hokey1.health, 100);
    assert_eq!(hokey1.position, (100.0, 200.0, 0.0));

    assert_eq!(hokey2.name, "Hokey II");
    assert_eq!(hokey2.height, 30);
    assert_eq!(hokey2.health, 100);
    assert_eq!(hokey2.position, (100.0, 200.0, 0.0));

    /* Example 2: Tuple-Like Structs */
    /* The second kind of struct type is called a tuple-like struct, because it ressembles a tuple*/

    /* You construct a value of this type mych as you would construct a typle, except that you
     * must include the struct name */
    let image_bounds = Bounds(1024, 768);
    assert_eq!(image_bounds.0 * image_bounds.1, 786432);

    /* Example 3: Struct Layout */
    let mut q = Queue { older: Vec::new(), younger: Vec::new() };

    q.push('0');
    q.push('1');
    assert_eq!(q.pop(), Some('0'));

    q.push('∞');
    assert_eq!(q.pop(), Some('1'));
    assert_eq!(q.pop(), Some('∞'));
    assert_eq!(q.pop(), None);

    assert!(q.is_empty());
    q.push('~');
    assert!(!q.is_empty());
    assert_eq!(q.pop(), Some('~'));
    assert!(q.is_empty());

    q.push('P');
    q.push('D');
    assert_eq!(q.pop(), Some('P'));
    q.push('X');

    let (older, younger) = q.split();
    // q is now uninitialized
    assert_eq!(older, vec!['D']);
    assert_eq!(younger, vec!['X']);

    /* Tuple-Like Structs can also use impl block and define associated functions */
    assert_eq!(image_bounds.size(), 786432);
    
    /* Example 4 : Passing Self as Box, Rc or Arc */
    let mut bq = Box::new(Queue::new());
    /*`Queue::push`expects a `mut& Queue` but `bq` is a `Box<Queue>`.
     * This is fine: Rusts borrows a `&mut Queue` from the `Box` for the
     * duration of the call */
    bq.push('#');
    assert_eq!(bq.pop(), Some('#'));

    /* Example 5: Associated Consts */
    /* These values are associated with the type itself, and you can use them without refering to
     * another instance of Vector2. Much like associated functions, they are accessed by naming the
     * type with which they're associated, followed by their name: */
    let unit = Vector2::UNIT;
    assert_eq!(unit.x, 1.0);
    assert_eq!(unit.y, 0.0);

    let zero= Vector2::ZERO;
    assert_eq!(zero.x, 0.0);
    assert_eq!(zero.y, 0.0);

    /* Example 6: Generic Structs */
    /* For associated function calls, you can supply the type parameter explicitly using the ::<>
     * (turbofish) notation, so QueueGen::<f64>::new() for example. But in practice, you can
     * usually just let Rust figure it out for you */
    let mut sq = QueueGen::new();
    let mut r = QueueGen::new();
    sq.push("CAD"); // apparently a QueueGen<&'static str>
    r.push(0.74); // apparently a QueueGen<f64>

    assert_eq!(sq.pop(), Some("CAD"));
    assert!(sq.is_empty());
    
    assert_eq!(r.pop(), Some(0.74));
    assert!(r.is_empty());

    let mut f = QueueGen::new();
    f.push(1.0);
    f.push(2.0);
    assert_eq!(f.sum(), 3.0);
    
    let (o, y) = f.split();
    // f is now uninitialized
    assert_eq!(o, vec![]);
    assert_eq!(y, vec![1.0, 2.0]);

    /* Example 7: Generic Structs with Lifetime Parameters */
    /* Here since find_extrama borrows elements of slice, which has lifetime 's, the Extrema struct
     * we return also use 's as the lifetime its references. Rust always infers lifetime parameters
     * for calls, so calls find_extrama needn't mention them: */
    let a = [0, -3, 0, 15, 48];
    let e = find_extrama(&a);
    assert_eq!(*e.least, -3);
    assert_eq!(*e.greatest, 48);

    /* Example 8: Generic Structs with Constant Parameters */
    /* A generic struct can also take parameters that are constant values. For example, you could
     * define a type representing polynomials of arbitrary degree.
     * With this definition Polynomial<6> is a sixtic polynomal, for example.*/
    use std::f64::consts::FRAC_PI_2; // n/2

    /* As with the type and lifetime parameters, Rust can often infer the right values for constant
     * parameters*/
    let sine_poly = Polynomial::new([0.0, 1.0, 0.0 , -1.0/6.0, 0.0,
                                     1.0/120.0]);
    assert_eq!(sine_poly.eval(0.0), 0.0);
    assert!((sine_poly.eval(FRAC_PI_2) - 1.).abs() < 0.005);

    /* Example 9 : Interior Mutability */
    /* Suppose for some reasons that we have an immutable shared data structure (this one), but we
     * want to add some logging facilities to it (which would require a &mut File) and an hardware
     * error counting mechanism. We need a little bit of mutable data (a File and a counter) inside
     * an otherwise immutable value (the SpiderRobot struct). This is called interior mutability.
     * Rust offers severals flavors of it: we will discuss and test Cell<T> and RefCell<T>.*/
    let robot = SpiderRobot::new("MyRobot".to_string(), false, "./myfile.log".to_string());
    robot.add_hardware_error();
    robot.log("bip bip bop");

    assert_eq!(robot.species, "MyRobot".to_string());
    assert_eq!(robot.web_enabled, false);
    assert!(robot.has_hardware_error());
    assert!(remove_file("./myfile.log").is_ok());
}
