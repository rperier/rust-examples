use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::thread;

fn main() {
    let flag = Arc::new(AtomicBool::new(false));

    let part = flag.clone();
    let handle = thread::spawn(move || {
        loop {
            if part.load(Ordering::Relaxed) {
                println!("ending");
                return;
            }
        }    
    });

    flag.store(true, Ordering::Relaxed);
    println!("waiting for looping thread");
    handle.join().unwrap();
}
