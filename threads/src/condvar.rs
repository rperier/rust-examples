use std::sync::{Arc, Condvar, Mutex};
use std::thread;

fn main() {
    // Let's implement the producer/consumer paradigm to introduce the use of condvar
    let data = String::with_capacity(4);
    let condition = Arc::new((Mutex::new(data), Condvar::new()));
    let mut thread_handles = vec![];

    // Produce, wait for a reply and display the reply
    let part = condition.clone();
    thread_handles.push(thread::spawn(move || {
        for _ in 0..5 {
            let (lock, condvar) = &*part;
            let mut data = lock.lock().unwrap();
            *data = "ping".to_string();
            condvar.notify_one();
            println!("producer: produced {data}, waiting reply...");
            while *data != "pong" {
                data = condvar.wait(data).unwrap();
            }
            println!("producer: got reply {data}");
        }
    }));

    // Consume and send a reply
    let part = condition.clone();
    thread_handles.push(thread::spawn(move || {
        for _ in 0..5 {
            let (lock, condvar) = &*part;
            let mut data = lock.lock().unwrap();

            while *data != "ping" {
                data = condvar.wait(data).unwrap();
            }
            println!("consumer: received {data}, sending back 'pong'");
            *data = "pong".to_string();
            condvar.notify_one();
        }
    }));

    for handle in thread_handles {
        handle.join().unwrap();
    }
}
