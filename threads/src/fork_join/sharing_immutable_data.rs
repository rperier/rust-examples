use std::iter;
use std::ops::Range;
use std::sync::Arc;
use std::thread;

fn prime(v: &Vec<i32>, range: Range<usize>) -> Vec<i32> {
    let mut res = vec![];

    for e in v[range].iter() {
        let mut is_prime = true;
        if *e <= 1 {
            continue;
        }
        for n in 2..*e {
            if *e % n == 0 {
                is_prime = false;
                break;
            }
        }
        if is_prime {
            res.push(*e);
        }
    }
    res
}

fn main() {
    // The Fork/Join parallelism
    /* Suppose that we have a Vector of numbers and you want to extract the prime numbers
     * from there. This time you can share the vector by using an Arc and ensure
     that the data does not outlive the thread lifetime */
    const NTHREADS: usize = 8;
    let numbers: Vec<_> = iter::successors(Some(1), |n| Some(n + 1)).take(32_768).collect();
    let numbers = Arc::new(numbers);
    let mut thread_handles = vec![];

    for id in 0..NTHREADS {
        let part = numbers.clone();
        let len = numbers.len() / NTHREADS;
        let range = Range { start: id * len, end: id * len + len };
        thread_handles.push(thread::spawn(move || prime(&part, range)));
    }
    let mut result = 0;
    for handle in thread_handles {
        result += handle.join().unwrap().len();
    }
    println!("{result} primes numbers found");
}
