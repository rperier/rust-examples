use std::iter;
use std::thread;

fn split_slice_into_chunks<T: Clone>(slice: Vec<T>, chunk_size: usize) -> Vec<Vec<T>> {
    slice.chunks(slice.len() / chunk_size).map(|e| e.into()).collect()
}

fn prime(v: Vec<i32>) -> Vec<i32> {
    let mut res = vec![];

    for e in v.iter() {
        let mut is_prime = true;
        if *e <= 1 {
            continue;
        }
        for n in 2..*e {
            if *e % n == 0 {
                is_prime = false;
                break;
            }
        }
        if is_prime {
            res.push(*e);
        }
    }
    res
}

fn main() {
    // The Fork/Join parallelism
    /* Suppose that we have a Vector of numbers and you want to extract the prime numbers
     * from there. You can split the work into multiple worklist and "collect" the result
     * of each thread with join */
    const NTHREADS: usize = 8;
    let numbers: Vec<_> = iter::successors(Some(1), |n| Some(n + 1)).take(32_768).collect();
    // For now we are split the initial Vec into a Vec of Vec<i32> and then we pass owership
    // to the threads
    let worklists = split_slice_into_chunks(numbers, NTHREADS);

    let mut thread_handles = vec![];

    for worklist in worklists {
        thread_handles.push(thread::spawn(|| prime(worklist)));
    }

    let mut result = 0;
    for handle in thread_handles {
        result += handle.join().unwrap().len();
    }
    println!("{result} primes numbers found");
}
