use std::sync::atomic::{AtomicBool, Ordering};
use std::thread;

static FLAG: AtomicBool = AtomicBool::new(false);

fn main() {
    let handle = thread::spawn(move || loop {
        if FLAG.load(Ordering::Relaxed) {
            println!("ending");
            return;
        }
    });

    FLAG.store(true, Ordering::Relaxed);
    println!("waiting for looping thread");
    handle.join().unwrap();
}
