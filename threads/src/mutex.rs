use std::sync::Arc;
use std::sync::Mutex;
use std::thread;

fn main() {
    // Shared mutable state
    const NTHREADS: usize = 8;
    let counter = 0;
    let counter = Arc::new(Mutex::new(counter));
    let mut thread_handles = vec![];

    for _ in 0..NTHREADS {
        let part = counter.clone();
        thread_handles.push(thread::spawn(move || {
            let mut guard = part.lock().unwrap();
            *guard += 1;
        }));
    }
    for handle in thread_handles {
        handle.join().unwrap();
    }
    let counter = Arc::into_inner(counter).unwrap().into_inner().unwrap();
    println!("counter: {counter}");
}
