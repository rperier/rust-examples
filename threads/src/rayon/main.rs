use rayon::prelude::*;
use std::iter;

fn prime(v: &[i32]) -> Vec<i32> {
    let mut res = vec![];

    for e in v.iter() {
        let mut is_prime = true;
        if *e <= 1 {
            continue;
        }
        for n in 2..*e {
            if *e % n == 0 {
                is_prime = false;
                break;
            }
        }
        if is_prime {
            res.push(*e);
        }
    }
    res
}

fn main() {
    // The workloads Parallism approach
    /* Suppose that we have a Vector of numbers and you want to extract the prime numbers
     * from there. This time it is done with rayon. Behind the scenes, Rayon balances workloads
     * across threads dynamically, using a technique call `work-stealing`. Rayon maps a work thread
     * per CPU core. Which is more efficient when the amount of work is not regular (from the computing
     * point of view)
     */
    let numbers: Vec<_> = iter::successors(Some(1), |n| Some(n + 1)).take(32_768).collect();
    let work: Vec<&[i32]> = numbers.chunks(1).collect();
    let result: usize = work.par_iter().map(|number| prime(number).len()).sum();
    println!("{result} primes numbers found");
}
