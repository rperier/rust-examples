use std::sync::Arc;
use std::sync::RwLock;
use std::thread;

fn main() {
    // We use a bank account to demonstrate the use of read-write locks.
    const NTHREADS: usize = 8;
    let checking_balance = 0f32;
    let checking_balance = Arc::new(RwLock::new(checking_balance));
    let mut thread_handles = vec![];

    // N readers
    for id in 0..NTHREADS {
        let part = checking_balance.clone();
        thread_handles.push(thread::spawn(move || {
            let guard = part.read().unwrap();
            println!("{id}: checking_balance is {guard}");
        }));
    }

    // One writer with many deposits
    for _ in 0..10 {
        let mut guard = checking_balance.write().unwrap();
        *guard += 1.5;
    }

    for handle in thread_handles {
        handle.join().unwrap();
    }
    let balance = checking_balance.read().unwrap();
    println!("balance: {balance}");
}
