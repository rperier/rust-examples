use std::iter;
use std::ops::Range;
use std::thread;

fn prime(v: &Vec<i32>, range: Range<usize>) -> Vec<i32> {
    let mut res = vec![];

    for e in v[range].iter() {
        let mut is_prime = true;
        if *e <= 1 {
            continue;
        }
        for n in 2..*e {
            if *e % n == 0 {
                is_prime = false;
                break;
            }
        }
        if is_prime {
            res.push(*e);
        }
    }
    res
}

fn main() {
    // The Fork/Join parallelism
    /* Suppose that we have a Vector of numbers and you want to extract the prime numbers
     * from there. This time with scope we are sure that the shared reference cannot outlive
     * the threads, because it is joined manually, but in the worst case it is joined implicitly
     * at the end of the scope */
    const NTHREADS: usize = 8;
    let numbers: Vec<_> = iter::successors(Some(1), |n| Some(n + 1)).take(32_768).collect();

    let result = thread::scope(|s| {
        let mut handles = vec![];

        for id in 0..NTHREADS {
            let len = numbers.len() / NTHREADS;
            let range = Range {start: id * len, end: id * len + len };
            handles.push(s.spawn(|| prime(&numbers, range)));
        }
        let mut result = 0;
        for handle in handles {
            result += handle.join().unwrap().len();
        }
        result
    });
    println!("{result} primes numbers found");
}
