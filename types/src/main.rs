// Iterate slices on the same way
fn find_pair(v: &[u8]) -> Vec<u8> {
    let mut r: Vec<u8> = Vec::new();

    for n in v {
        if n % 2 == 0 {
            r.push(*n);
        }
    }
    r
}

fn find_pattern(s: &str, pattern: &str) -> Option<usize> {
    s.find(pattern)
}

fn vec_array_slices() {
    println!("Vector, array and slices examples:");
    // With a Vec<u8>
    //let v = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    // Or with an array of u8
    let v: [u8; 10] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    // Find pair numbers for the entire vector or array
    let res = find_pair(&v);
    for n in res {
        println!("{n}");
    }
    println!("----");
    
    // Or even for the last 4th elements
    let res = find_pair(&v[6..10]);
    for n in res {
        println!("{n}");
    }
}

fn slice_string_and_strings() {
    println!("String litteral, Strings and string slices examples:");
    let noodles = "noodles".to_string();
    let oodles = &noodles[1..];

    let idx = match find_pattern(&noodles, "dl") {
        None => {
            eprintln!("No such pattern found");
            return;
        },
        Some(idx) => idx
    };
    println!("looking for pattern \"dl\" in the \"noodles\" String, found it at {}", idx);

    let id = match find_pattern(oodles, "dl") {
        None => {
            eprintln!("No such pattern found");
            return;
        },
        Some(idx) => idx
    };
    println!("looking for pattern \"dl\" in the \"oodles\" String slice, found it at {}", id);
}

fn main() {
    vec_array_slices();
    slice_string_and_strings();
}
