use std::ops::{Deref, DerefMut, Drop};
use std::net::{Ipv4Addr, Ipv6Addr};

struct Appelation {
    name: String,
    nicknames: Vec<String>,
}

impl Drop for Appelation {
    fn drop(&mut self) {
        print!("Dropping {}", self.name);
        if !self.nicknames.is_empty() {
            print!(" (AKA {})", self.nicknames.join(", "));
        }
        println!();
    }
}

struct MyBox<T> {
    ptr: Box<T>,
}

impl<T> MyBox<T> {
    fn new(value: T) -> Self {
        Self { ptr: Box::new(value) }
    }
}

impl<T> Deref for MyBox<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.ptr
    }
}

impl<T> DerefMut for MyBox<T> {
    fn deref_mut(&mut self) -> &mut T {
        &mut self.ptr
    }
}

impl AsRef<String> for Appelation {
    fn as_ref(&self) -> &String {
        &self.name
    }
}

#[derive(Default)]
struct Selector<T> {
    /// Elements available in this `Selector`.
    elements: Vec<T>,

    /// The index of the "current" element in `elements`. A `Selector`
    /// behaves like a pointer to the current element.
    current: usize,
}

impl<T> Deref for Selector<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.elements[self.current]
    }
}

impl<T> DerefMut for Selector<T> {
    fn deref_mut(&mut self) -> &mut T {
        &mut self.elements[self.current]
    }
}

fn display_coerced(value: &str) {
    println!("{value}");
}

fn display_appelation_asref<T: AsRef<String>> (value: &T)
{
    println!("{}", value.as_ref());
}

fn ping<A>(address: A) -> std::io::Result<bool>
    where A: Into<Ipv4Addr>
{
    println!("ping {}", address.into());
    Ok(true)
}

use std::path::PathBuf;
use std::borrow::Cow;

enum Error {
    OutOfMemory,
    StackOverflow,
    MachineOnFire,
    Unfathomable,
    FileNotFound(String)
}

fn describe(error: &Error) -> Cow<'static, str> {
    match *error {
        Error::OutOfMemory => "out of memory".into(),
        Error::StackOverflow => "stack overflow".into(),
        Error::MachineOnFire => "machine on fire".into(),
        Error::Unfathomable => "machine bewildered".into(),
        Error::FileNotFound(ref path) => {
            format!("file not found {}", path).into()
        } 
    }   
}

fn main() {
    /* Drop */

    // A drops occurs under a variety of circumstances: when a variable goes or of scope,
    // at the end of an expression statement; when you truncate a vector, removing elements from
    // it ends, and so on.
    // For the most parts, Rust handles dropping values for you automatically, For example suppose
    // the following type (Appelation):
    {
        let mut a = Appelation { name: "Zeus".to_string(), nicknames: vec![ "cloud collector".to_string(), "kind of the gods".to_string() ] };
        println!("before assignment");
        // a is dropped before "Hera" is assigned
        a = Appelation { name: "Hera".to_string(), nicknames: vec![], };
        println!("at end of block");
    } // end of scope, both are dropped

    /* Clone : TODO */

    /* Copy: TODO */

    /* Deref and DerefMut */

    // You can specify how deferencing operators like * and . behave on your types
    // by implemeting the std::ops::Deref and std::ops::DerefMut traits. Pointer
    // types like Box<T> or Rc<T> implement these traits so they can behave like
    // Rust's built-in pointer types do. For example, if you have a Box<Complex>
    // value b, then *b refers to the Complex value that b point to, and b.re
    // refers to its real component.

    // But Deref and DerefMut can also do things called "coercions".

    // Simple Deref and coercions Using a Box like structure.
    // Both can be coerced to an &str, because String implements
    // the Deref and DerefMut traits
    let b = MyBox::new("hello world");
    display_coerced(&b);

    let c = MyBox::new("another value".to_string());
    display_coerced(&c);

    let mut d: &str = &c;
    d = "huhu";
    display_coerced(d);

    // Given the Selector implementation, you can use a Selector like this:
    let mut s = Selector { elements: vec!['x', 'y', 'z'], current: 2 };

    // Because `Selector` implements `Deref`, we can use the `*` operator to
    // refer to its current element.
    assert_eq!(*s, 'z');

    // Assert that 'z' is alphabetic, using a method of `char` directly on a
    // `Selector`, via deref coarcion.
    assert!(s.is_alphabetic());

    // Change the 'z' to a 'w', by assigning to the `Selector`'s referent.
    *s = 'w';

    assert_eq!(s.elements, ['x', 'y', 'w']);

    // With Deref or DerefMut Target should be something that Self contains, owns or refers to: Box<Complex>
    // for example, the Target type is Complex. The Deref and DerefMut traits are designed for implementing
    // smart pointer types, likes Box, Rc and Arc, and type that serve as owning versions of something you
    // would also frequently use by reference, the way Vec<T> and String serve as owning versions of [T] and
    // str. You should not implement Deref and DerefMut for a type just to make the Target type's method appear
    // on it automatically, the way a C++ base class's methods are visible on a subclass. This will not always work
    // as you expect and can be confusing when it goes awry.

    /* Default */

    // Some types have a reasonably obvious default value: The default vector or string is empty,
    // the default number is zero, the default Option is None, and so on. Types like this can implement
    // the std::default::Default trait.
    // The String's implementation of Default is simple, the default method simply returns a fresh value
    // of Self, in our case string::new().

    // All of Rust's collection types: Vec, HashMap, BinaryHeap, and so on, implement Default with default methods that
    // return an empty collection. This is helpful when you need to build a collector of values but want to let your caller
    // decide exactly what sort of collection to build. For example, the Iterator trait's partition method splits the values
    // the iterator produces into two collections, using a closure to device where each value goes:

    use std::collections::HashSet;

    let squares = [4, 9, 16, 25, 36, 49, 64];
    let (powers_of_two, impure): (HashSet<i32>, HashSet<i32>) =
        squares.iter().partition(|&n| n & (n - 1) == 0);

    assert_eq!(powers_of_two.len(), 3);
    assert_eq!(impure.len(), 4);

    // But of course, partition is not specific to HashSets, you can use it to produce any sort of collection you like,
    // as long as the collection type implements Default, to produce an empty collection to start with, and Extend<T>, to
    // add a T to the collection. String implements Default and Extend<char>, so you can write:
    let (upper, lower): (String, String) = "Great Teacher Onizuka".chars().partition(|&c| c.is_uppercase());
    assert_eq!(upper, "GTO");
    assert_eq!(lower, "reat eacher nizuka");

    // Another common use of Default is to produce default values for structs fields
    let selector = Selector { elements: vec!["hello", "world"], ..Default::default()};
    assert_eq!(*selector, "hello");

    /* AsRef and AsMut */

    // When a type implements AsRef<T>, that means you can borrow a &T from it efficiently. AsMut is the analogue for mutable
    // references. So for example, Vec<T> implements AsRef<T>, and string implements AsRef<str>. You can also borrow a string's
    // contents as an array of bytes, so String implements AsRef<[u8]> as well.

    // AsRef is used to make functions more flexible in the arguments types they accept. For example, the std::fs::File::open
    // function is declared like this:
    //
    // fn open<P: AsRef<Path>>(path: P) -> Result<File>
    //
    // What open really wants is a &Path, the type representing a filesystem path. But with this signature, open accepts anything it can
    // borrow a &Path from, that is, anything that implements AsRef<Path>. Such type include String and str, the operating system interface
    // string types OsString and OsStr, and of course PathBuf and Path; see the library documentation for the full list. This is what allow
    // you to pass string literals to open:
    
    use std::fs::File;
    let f = File::open("Cargo.toml");
    if let Err(e) = f {
        eprintln!("Something went wrong {}", e);
    }

    // All of he standard library's filesystem access functions accept path arguments this way. For callers, the effect resembles that of an overloaded function
    // in C++, althought Rust takes a different approach toward establishing which argument types are acceptable. But this can't be the whole story. A string
    // literal is a &str, but the type that implements AsRef<Path> is str, without &. And as we explained in "Deref and DerefMut", Rust does not try deref
    // coercoins to satisfy type variables bounds, so they won't help here either.
    //
    // Fortunately, the standard library includes the blanket implementation:
    //
    // impl<'a, T, u> AsRef<U> for &'a T
    // where T: AsRef<U>
    //       T: ?Sized, U: ?Sized
    // {
    //   fn as_ref(&self) -> &U { 
    //     (*self).as_ref()
    //   }
    // }
    //
    // In other words, for any types T and U, if T: AsRef<U>, then &T: AsRef<U> as well: simply follow the reference and proceed as before. In particular, since
    // str: AsRef<Path>, then &str: AsRef<Path> as well, this is why the example with File::open() above works. In a sense, this is a way to get limited form of
    // deref coercion in checking AsRef bounds on type variables. The following code works, because Appelation implements AsRef<String>.

    let a = Appelation { name: "Zeus".to_string(), nicknames: vec![ "cloud collector".to_string(), "kind of the gods".to_string() ] };
    display_appelation_asref(&a);

    // You might assume that if a type implements AsRef<T>, it should also implement asMut<T>. However, there are cases where it is not appropriate. For example,
    // we've mentioned that String implements AsRef<[u8]>; this makes sense, but you don't want to want to being able to borrow a &mut [u8] from a String, as
    // all of those byte are guaranteed to be correct UTF-8 bytes. So you must implements AsMut<T> explicitly.

    /* Borrow and BorrowMut */

    // See official documentation: https://doc.rust-lang.org/std/borrow/trait.Borrow.html

    /* From and Into */

    // Into
    println!("{:?}", ping(Ipv4Addr::new(23, 24, 68, 141))); // pass an Ipv4Addr
    println!("{:?}", ping([66, 146, 219, 98])); // pass a [u8; 4]
    println!("{:?}", ping(0xd076eb94_u32)); // pass a u32

    // The From trait, however plays a different role. The from method serves as a generic constructor for producting an instance of a type from some other single
    // value. For example, rather than Ipv4Addr having two methods named from_arrays and from_u32, it simply implements From<[u8;4]> and From<u32>, allowing us to
    // write:
    let addr1 = Ipv4Addr::from([66, 146, 219, 90]);
    let addr2 = Ipv4Addr::from(0xd076eb94_u32);

    println!("{addr1}");
    println!("{addr2}");

    let text = "Beautiful Soup".to_string(); // conversion with allocation and copy
    let bytes: Vec<u8> = text.into(); // cheap conversion , the underlying buffer is moved

    println!("{bytes:?}");

    let huge = 2_000_000_000_000i64;
    {
    let smaller = huge as i32;
    assert_eq!(smaller, -1454759936);
    }

    /* TryFrom and TryInto */

    let smaller: i32 = huge.try_into().unwrap_or_else(|_| {
        if huge >= 0 {
            i32::MAX
        } else {
            i32::MIN
        }
    });

    assert_eq!(smaller, i32::MAX);

    // Where From and Into relate types with simple conversions, TryFrom and TryInto extend the simplicity of From and Into conversions with the expressive
    // error handling afforded by Result. These four traits can be used together to relate many types in a single crate.

    /* TooOwned */

    // Given a refrence, the usual way to produce an owned copy of its referent is to call clone, assuming the type implements std::clone::Clone. But what if
    // you want to clone &str or &[i32] ? Clone works only for going from &T to T. The ToOwned trait generalizes Clone to construct owned data from any borrow
    // of a given type. With the previous example if you want ot clone &str or a &[i32], what you probably want is a String or Vec<i32>, but clone's definition
    // does not permet that, as it always return a type T from a &T, and str and [u8] are unsigned; they aren't even types that a function could return.
    // The std::borrow::ToOwned trait provides a slightly looser way to convert a reference to an owned value:
    //
    // trait ToOwned {
    //   type Owned: Borrow<Self>;
    //   fn to_owned(&self) -> Self::Owned;
    // }

    // These type can be inferred at compile time, the type annotation is just for the example
    let s: &str = "a";
    let ss: String = s.to_owned();
    assert_eq!(s, ss);

    // These type can be inferred at compile time, the type annotation is just for the example
    let v: &[i32] = &[1, 2];
    let vv: Vec<i32> = v.to_owned();
    assert_eq!(v, vv);

    let mut sss: String = String::new();
    "hello".clone_into(&mut sss);

    let mut vvv: Vec<i32> = Vec::new();
    [1, 2][..].clone_into(&mut vvv);

    /* Cow: Borrow and ToOwned at Work: The Humble Cow */

    // pub enum Cow<'a, B>
    // where
    //   B: 'a + ToOwned + ?Sized,
    // {
    //   Borrowed(&'a B),
    //   Owned(<B as ToOwned>::Owned),
    // }

    // The type Cow is a smart pointer providing clone-on-write functionality: it can enclose and provide immutable
    // access to borrowed data, and clone the data lazily when mutation or ownership is required. The type is designed
    // to work with general borrowed data via the Borrow trait. Cow implements Deref, which means that you can call
    // non-mutating methods directly on the data it encloses. If mutation is desired, to_mut will obtain a mutable 
    // reference to an owned value, cloning if necessary.
    //
    // A Cow<B> either borrows a shared reference to a B or owns a value from which we could borrow such a reference.
    // Since Cow implements Deref, you can call methods on it as if it were a shared reference to a B: if it's Owned,
    // it borrows a shared reference to the owned value; and if it's Borrowed, it just hands out the reference it's
    // holding. You can also get a mutable reference to a Cow's value by calling its to_mut method, which returns a &mut B.
    // If the Cow happens to be a Cow::Borrowed, to_mut simply calls the references's to_owned method to get its own copy
    // of the referent, changes the Cow into a Cow::Owned, and borrows a mutable reference to the newly owned value. This
    // is the "clone on write" behaviour the type's name refers to. Similarly, Cow has an into_owned method that promotes
    // the reference to an owned value, if necessary, and then returns it, moving ownership to the caller and consuming
    // the Cow in the process.

    // One common use for Cow is to return either a statically allocated string constant or a computed string. For example,
    // suppose you need to convert an error enum to a message. Most of the variants can be handled with a fixed strings, but
    // some of them have additional data that should be included in the message. You can return a Cow<'static, str>:

    let e = Error::MachineOnFire;
    println!("Disaster has struck: {}", describe(&e));

    let e2 = Error::FileNotFound(String::from("file.txt"));
    println!("Disaster has struck: {}", describe(&e2));

    // This code uses Cow's implementation of Into to construct the values. Most arms of this match statement return a Cow::Borrowed
    // referring to a statically allocated string. But when we get a FileNotFound variant, we use format! to construct a message
    // incorporating the given filename. This arm of the match statement produces a Cow::Owned value.
}
