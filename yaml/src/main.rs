use anyhow::{Context, Result};
use serde::Deserialize;
use std::fs;

#[derive(Deserialize, Debug)]
struct Mytype<'a> {
    master_network_interface: &'a str,
    ip_address: &'a str,
    trusted_ca: &'a str,
    certificate_name: &'a str,
    key_name: &'a str,
    execution_period: u16,
}

fn main() -> Result<()> {
    let data = fs::read_to_string("./config.yaml").with_context(|| format!("Failed to read config.yaml"))?;
    let value: Mytype = serde_yaml::from_str(&data)?;

    println!("{:?}", value);
    Ok(())
}
